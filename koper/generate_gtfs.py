from pathlib import Path
from datetime import date, datetime, timedelta
import time
import shutil
import tempfile
import json
import zipfile
import tqdm
import requests
import pandas as pd
import csv
import hashlib
import os


OUTPUT = Path('mok_gtfs.zip').absolute()
CACHE_DIR = tempfile.TemporaryDirectory()
os.chdir(CACHE_DIR.name)

class Route:
    def __init__(self, route_id, route_short_name, route_long_name, route_type, route_color, route_text_color, agency_id='MPK'):
        self.route_id = route_id
        self.route_short_name = route_short_name
        self.route_long_name = route_long_name
        self.route_type = route_type
        self.route_color = route_color
        self.route_text_color = route_text_color
        self.agency_id = agency_id

    def __repr__(self):
        return f'Route({self.route_id}, {self.route_short_name}, {self.route_long_name}, {self.route_type}, {self.route_color}, {self.route_text_color})'
    
    def __str__(self):
        return f'{self.route_id} - {self.route_short_name} - {self.route_long_name}'

class Trip:
    def __init__(self, route_id, service_id, trip_id, trip_headsign):
        self.route_id = route_id
        self.shape_id = route_id
        self.service_id = service_id
        self.trip_id = trip_id
        self.trip_headsign = trip_headsign

    def __repr__(self):
        return f'Trip({self.route_id}, {self.service_id}, {self.trip_id}, {self.trip_headsign})'
    
    def __str__(self):
        return f'{self.trip_id} - {self.trip_headsign}'
    
    def set_start_time(self, start_time):
        self.start_time = start_time
        
    def set_end_time(self, end_time):
        self.end_time = end_time
    
class StopTime:
    def __init__(self, trip_id, arrival_time, departure_time, stop_id, stop_sequence):
        self.trip_id = trip_id
        self.arrival_time = arrival_time
        self.departure_time = departure_time
        self.stop_id = stop_id
        self.stop_sequence = stop_sequence
        self.timepoint = 1

    def __repr__(self):
        return f'StopTime({self.trip_id}, {self.arrival_time}, {self.departure_time}, {self.stop_id}, {self.stop_sequence})'
    
    def __str__(self):
        return f'{self.trip_id} - {self.stop_id} - {self.arrival_time}'
    
class Shape:
    def __init__(self, shape_id, shape_pt_lat, shape_pt_lon, shape_pt_sequence):
        self.shape_id = shape_id
        self.shape_pt_lat = shape_pt_lat
        self.shape_pt_lon = shape_pt_lon
        self.shape_pt_sequence = shape_pt_sequence

    def __repr__(self):
        return f'Shape({self.shape_id}, {self.shape_pt_lat}, {self.shape_pt_lon}, {self.shape_pt_sequence})'
    
    def __str__(self):
        return f'{self.shape_id} - {self.shape_pt_lat} - {self.shape_pt_lon}'


day_start = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
day_plus_14 = day_start + timedelta(days=14)

# Create the agency
agency_df = pd.DataFrame([{
    'agency_id': 'MPK',
    'agency_name': 'Arriva MP Koper',
    'agency_url': 'https://arriva.si/',
    'agency_timezone': 'Europe/Ljubljana',
}])

# Get the stops
def get_timestamp_and_token() -> (str, str):
    """Generate the timestamp and token."""
    timestamp = datetime.now().strftime('%Y%m%d%H%M%S')
    token = hashlib.md5(f'R300_VozniRed_2015{timestamp}'.encode('utf-8')).hexdigest()
    return timestamp, token

timestamp, token = get_timestamp_and_token()

response = requests.get('https://prometws.alpetour.si/WS_ArrivaSLO_TimeTable_DepartureStations.aspx', params={
    'cTOKEN': token,
    'cTIMESTAMP': timestamp,
    'POS_NAZ': '',
    'JSON': '1',
    'SearchType': '2',
}, timeout=10)

response.raise_for_status()

stops = response.json()

ojpp_stops = requests.get('https://ojpp.si/api/stops/')
ojpp_stops.raise_for_status()
ojpp_stops = ojpp_stops.json()

ojpp_stops_list = [
    {
        'stop_id': str(stop['ijpp_id']),
        'stop_name': stop['name'],
        'stop_lat': stop['location'].split('(')[1].split(')')[0].split(' ')[1],
        'stop_lon': stop['location'].split('(')[1].split(')')[0].split(' ')[0],
    } for stop in ojpp_stops
]

# Fix Supernova, Mercator and Banka stops
stop_locations = {
    'Supernova': '45.5440721131994|13.7386220645608',
    'Mercator': '45.5298075570202|13.7334624943166',
    'Banka': '45.5439776439653|13.7303935540452',
    'Barka': '45.54645058487235|13.738000034126351'
}

for stop in ojpp_stops_list:
    if stop['stop_name'] in stop_locations:
        stop['stop_lat'], stop['stop_lon'] = stop_locations[stop['stop_name']].split('|')

stop_name_to_id_map = {
    stop['stop_name']: stop['stop_id'] for stop in ojpp_stops_list
}
# Remove lines without id
stops_df = pd.DataFrame(ojpp_stops_list).drop_duplicates().dropna(subset=['stop_id'])

stop_map = {
    stop['POS_NAZ']: stop['JPOS_IJPP'] for stop in stops[0]['DepartureStations']
}

route_list = []
trips_list = []
trips_map = {}
trip_ids_to_active_dates = {}
stop_times_list = []
shapes_list = []

koper_stop_pairs = [
    {
        'line_number': '1',
        'line_name': 'Koper - Kampel - Koper',
        'stop1': 'Koper',
        'stop2': 'Koper Šalara market',
        'has_line_number': True,
        'ignore_with_line_number': False,
        'color': '0eaa49',
    },
    {
        'line_number': '2',
        'line_name': 'Koper - Semedela - Markovec - Bolnica - Koper',
        'stop1': 'Koper',
        'stop2': 'Bol.Izola',
        'has_line_number': True,
        'ignore_with_line_number': False,
        'color': 'ee3533'
    },
    {
        'line_number': '2A',
        'line_name': 'Koper - Markovec - Bolnica - Koper',
        'stop1': 'Koper',
        'stop2': 'Bol.Izola',
        'has_line_number': True,
        'ignore_with_line_number': False,
        'color': '00b6f1'
    },
    {
        'line_number': '3',
        'line_name': 'Nad Dolinsko - Markovec - Bolnica - Nad Dolinsko',
        'stop1': 'Nad Dolinsko cesto',
        'stop2': 'Bol.Izola',
        'has_line_number': False,
        'ignore_with_line_number': True,
        'color': 'dbac6a'
    },
    {
        'line_number': '4',
        'line_name': 'Šalara - Olmo - Tržnica - Žusterna - Tržnica - Olmo - Šalara',
        'stop1': 'Koper Šalara market',
        'stop2': 'Mercator',
        'has_line_number': False,
        'ignore_with_line_number': True,
        'color': '00becd'
    },
    {
        'line_number': '5',
        'line_name': 'Koper Brolo - Žusterna - Markovec - Žusterna - Koper Brolo',
        'stop1': 'Koper Šalara market',
        'stop2': 'Markovec-Center Za Gradom',
        'has_line_number': False,
        'ignore_with_line_number': True,
        'color': '8fa3d4'
    },
    {
        'line_number': '6',
        'line_name': 'Potniški terminal - Žusterna - Markovec - Žusterna - Potniški terminal',
        'stop1': 'Potniški terminal',
        'stop2': 'Markovec-Center Za Gradom',
        'has_line_number': False,
        'ignore_with_line_number': True,
        'color': '8fa3d4'
    },
    {
        'line_number': '7',
        'line_name': 'Potniški terminal - Kraljeva - Rozmanova - Potniški terminal',
        'stop1': 'Potniški terminal',
        'stop2': 'Ulica II. prekomorske brigade',
        'has_line_number': False,
        'ignore_with_line_number': True,
        'color': '005bab'
    },
    {
        'line_number': '8',
        'line_name': 'Potniški terminal - Rozmanova - Olmo - Potniški terminal',
        'stop1': 'Potniški terminal',
        'stop2': 'Rozmanova ulica I',
        'has_line_number': False,
        'ignore_with_line_number': True,
        'color': 'dbe11f',
        'text': '000000'
    },
    {
        'line_number': '9',
        'line_name': 'Koper AP - Potniški terminal - Koper AP',
        'stop1': 'Koper',
        'stop2': 'Potniški terminal',
        'has_line_number': False,
        'ignore_with_line_number': True,
        'color': 'f38221'
    },
    {
        'line_number': '10',
        'line_name': 'Srmin GORC - Tržnica - Srmin GORC',
        'stop1': 'Koper Sv. Ana V.n.',
        'stop2': 'Barka',
        'has_line_number': False,
        'ignore_with_line_number': True,
        'color': '2b3192'
    },
    {
        'line_number': '11',
        'line_name': 'Cimos P&R - Potniški terminal - CIMOS P&R',
        'stop1': 'Cimos P&R',
        'stop2': 'Koper Tržnica',
        'has_line_number': False,
        'ignore_with_line_number': True,
        'color': '2b3192'
    },
    {
        'line_number': '12',
        'line_name': 'PH Sonce - Koper Brolo - PH Sonce',
        'stop1': 'Cimos P&R',
        'stop2': 'Koper Sv. Ana V.n.',
        'has_line_number': False,
        'ignore_with_line_number': True,
        'color': '911d8b'
    },
]

def create_koper_routes():
    for pair in koper_stop_pairs:
        route_id = f'MPK_{pair["line_number"]}'
        route_list.append(Route(
            route_id=route_id,
            route_short_name=pair['line_number'],
            route_long_name=pair['line_name'],
            route_type=3,
            route_color=pair['color'],
            route_text_color=pair.get('text', 'FFFFFF'),
        ))

def fetch_koper(date):
    timestamp, token = get_timestamp_and_token()
    for pair in koper_stop_pairs:
        pair_directions = [
            (pair['stop1'], pair['stop2']),
            (pair['stop2'], pair['stop1']),
        ]
        for direction in pair_directions:
            response = requests.get('https://prometws.alpetour.si/WS_ArrivaSLO_TimeTable_TimeTableDepartures.aspx', params={
                'cTOKEN': token,
                'cTIMESTAMP': timestamp,
                'JPOS_IJPPZ': stop_map[direction[0]],
                'JPOS_IJPPK': stop_map[direction[1]],
                'VZVK_DAT': date,
                'JSON': '1',
                'SearchType': '2',
            }, timeout=10)
            response.raise_for_status()
            timetable = response.json()
            for departure in timetable[0]['Departures']:
                line_no = departure['ROD_OPO'].split(' ')[1] if len(departure['ROD_OPO'].split(' ')) > 1 else ''
                if pair['has_line_number'] and line_no != pair['line_number']:
                    # print('          Skipping', pair['line_number'], direction[0], direction[1])
                    continue
                if not pair['has_line_number'] and pair['ignore_with_line_number'] and line_no != '':
                    # print('          Skipping', pair['line_number'], direction[0], direction[1])
                    continue
                trip_id = f"{departure['REG_ISIF']}_{departure['OVR_SIF']}"
                if not trip_ids_to_active_dates.get(trip_id):
                    trip_ids_to_active_dates[trip_id] = []
                if date not in trip_ids_to_active_dates[trip_id]:
                    trip_ids_to_active_dates[trip_id].append(date)
                if trip_id in trips_map:
                    # print('          Skipping', pair['line_number'], direction[0], direction[1])
                    continue
                # print('          Fetching', pair['line_number'], direction[0], direction[1])
                trip = Trip(
                    route_id=f'MPK_{pair["line_number"]}',
                    service_id=date,
                    trip_id=trip_id,
                    trip_headsign=''
                )
                trips_map[trip_id] = trip

                trips_list.append(trip)

                stop_times_for_trip = requests.get('https://prometws.alpetour.si/WS_ArrivaSLO_TimeTable_TimeTableDepartureStationList.aspx', params={
                    'cTOKEN': token,
                    'cTIMESTAMP': timestamp,
                    'SPOD_SIF': departure['SPOD_SIF'],
                    'REG_ISIF': departure['REG_ISIF'],
                    'VVLN_ZL': departure['VVLN_ZL'],
                    'ROD_ZAPZ': departure['ROD_ZAPZ'],
                    'ROD_ZAPK': departure['ROD_ZAPK'],
                    'OVR_SIF': departure['OVR_SIF'],
                    'JSON': '1',
                    'SearchType': '2',
                }, timeout=10)

                stop_times_for_trip.raise_for_status()

                stop_times = stop_times_for_trip.json()
                for stop_time in stop_times[0]['DepartureStationList']:
                    stop_times_list.append(StopTime(
                        trip_id=trip_id,
                        arrival_time=f"{stop_time['ROD_IPRI']}:00" if stop_time['ROD_IPRI'] != '' else f'{stop_time["ROD_IODH"]}:00',
                        departure_time=f"{stop_time['ROD_IODH']}:00" if stop_time['ROD_IODH'] != '' else f'{stop_time["ROD_IPRI"]}:00',
                        stop_id=stop_name_to_id_map[stop_time['POS_NAZ']],
                        stop_sequence=stop_time['ROD_ZAP'],
                    ))
                if stop_times[0]['DepartureStationList'][0]['POS_NAZ'] != stop_times[0]['DepartureStationList'][-1]['POS_NAZ']:
                    trip.trip_headsign = f"{stop_times[0]['DepartureStationList'][0]['POS_NAZ']} - {stop_times[0]['DepartureStationList'][-1]['POS_NAZ']}"
                else:
                    middle_stop = stop_times[0]['DepartureStationList'][len(stop_times[0]['DepartureStationList']) // 2]
                    trip.trip_headsign = f"{stop_times[0]['DepartureStationList'][0]['POS_NAZ']} - {middle_stop['POS_NAZ']} - {stop_times[0]['DepartureStationList'][-1]['POS_NAZ']}"
                b = 0
            print('     Fetched', pair['line_number'], direction[0], direction[1])


create_koper_routes()

for date_ in pd.date_range(day_start, day_plus_14):
    date_str = date_.strftime('%Y-%m-%d')
    print('Fetching', date_str)
    fetch_koper(date_str)

# Generate calendar from the trip to active dates mapping
calendar_list = []
calendar_dates_list = []
for trip_id, active_dates in trip_ids_to_active_dates.items():
    hash_ = hashlib.md5(','.join(active_dates).encode('utf-8')).hexdigest()
    calendar_list.append({
        'service_id': hash_,
        'monday': 0,
        'tuesday': 0,
        'wednesday': 0,
        'thursday': 0,
        'friday': 0,
        'saturday': 0,
        'sunday': 0,
        'start_date': day_start.strftime('%Y%m%d'),
        'end_date': day_plus_14.strftime('%Y%m%d'),
    })
    for active_date in active_dates:
        calendar_dates_list.append({
            'service_id': hash_,
            'date': active_date.replace('-', ''),
            'exception_type': 1,
        })
    # Update trip with service_id
    trips_map[trip_id].service_id = hash_


trips_df = pd.DataFrame([{
    'route_id': trip.route_id,
    'service_id': trip.service_id,
    'trip_id': trip.trip_id,
    'trip_headsign': trip.trip_headsign,
} for trip in trips_list])

routes_df = pd.DataFrame([{
    'route_id': route.route_id,
    'route_short_name': route.route_short_name,
    'route_long_name': route.route_long_name,
    'route_type': route.route_type,
    'route_color': route.route_color,
    'route_text_color': route.route_text_color,
    'agency_id': route.agency_id,
} for route in route_list])

stop_times_df = pd.DataFrame([{
    'trip_id': stop_time.trip_id,
    'arrival_time': stop_time.arrival_time,
    'departure_time': stop_time.departure_time,
    'stop_id': stop_time.stop_id,
    'stop_sequence': stop_time.stop_sequence,
    'timepoint': 1,
} for stop_time in stop_times_list])

calendar_df = pd.DataFrame(calendar_list).drop_duplicates()
calendar_dates_df = pd.DataFrame(calendar_dates_list).drop_duplicates()

# Drop unused stop_ids
stop_ids = set(stop_times_df['stop_id'])
stops_df = stops_df[stops_df['stop_id'].isin(stop_ids)]

# Save the GTFS files
trips_df.to_csv('trips.txt', index=False)
print('trips.txt saved')
stop_times_df.to_csv('stop_times.txt', index=False)
print('stop_times.txt saved')
#shapes_df.to_csv('shapes.txt', index=False)
#print('shapes.txt saved')
routes_df.to_csv('routes.txt', index=False)
print('routes.txt saved')
stops_df.to_csv('stops.txt', index=False)
print('stops.txt saved')
calendar_df.to_csv('calendar.txt', index=False)
print('calendar.txt saved')
calendar_dates_df.to_csv('calendar_dates.txt', index=False)
print('calendar_dates.txt saved')
agency_df.to_csv('agency.txt', index=False)
print('agency.txt saved')


# Write the GTFS into a zip file
with zipfile.ZipFile(OUTPUT, 'w') as zipf:
    zipf.write('agency.txt')
    zipf.write('stops.txt')
    zipf.write('calendar.txt')
    zipf.write('calendar_dates.txt')
    zipf.write('routes.txt')                    
    zipf.write('trips.txt')
    #zipf.write('shapes.txt')
    zipf.write('stop_times.txt')

print('GTFS saved to', OUTPUT)

b = 0