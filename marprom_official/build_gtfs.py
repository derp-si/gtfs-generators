from pathlib import Path
import tempfile
import traceback
import zipfile
import requests
import pandas as pd
import os
import re

OUTPUT = Path('marprom_gtfs.zip').absolute()
CACHE_DIR = tempfile.TemporaryDirectory()

os.chdir(CACHE_DIR.name)

INPUT = Path('marprom_gtfs_official.zip')

#################
#   LOAD GTFS   #
#################
print('Downloading GTFS...', end="")
resp = requests.get('http://gtfs.derp.si/marprom_gtfs_official.zip')
resp.raise_for_status()
assert len(resp.content) > 100000, 'GTFS file is suspiciously small!'
with INPUT.open('wb') as f:
    f.write(resp.content)
print('DONE')
    
# Unzip the GTFS
with zipfile.ZipFile(INPUT, 'r') as zipf:
    zipf.extractall()
    
# Load the routes into a DataFrame
routes = pd.read_csv('routes.txt')

print('Fetching line colors...')

extracted_colors = {}
try:
    # Fetch the CSS containing the line colors
    html = requests.get('https://vozniredi.marprom.si/')
    html.raise_for_status()
    html = html.text

    color_line_pattern = re.compile(r"--color:(#.+?);.+?>(.+?)<")

    for match in color_line_pattern.findall(html):
        extracted_colors[match[1]] = {
            # Remove hashtags, since GTFS is intolerant to them
            "route_color": match[0].replace('#', ''),
            "route_text_color": "FFFFFF",
        }
    print('DONE')
except Exception as e:
    print('Error fetching line colors')
    traceback.print_exc()

# Update the routes with the fetched colors
routes['route_color'] = routes['route_short_name'].map(lambda x: extracted_colors[x]['route_color'] if x in extracted_colors else '')
routes['route_text_color'] = routes['route_short_name'].map(lambda x: extracted_colors[x]['route_text_color'] if x in extracted_colors else '')

# Write the routes back to the file
routes.to_csv('routes.txt', index=False)

# Fix phone number for agency (remove spaces)
agency = pd.read_csv('agency.txt')
agency['agency_phone'] = ''
agency.to_csv('agency.txt', index=False)

# Write the GTFS into a zip file
with zipfile.ZipFile(OUTPUT, 'w') as zipf:
    zipf.write('agency.txt')
    zipf.write('stops.txt')
    zipf.write('calendar.txt')
    zipf.write('calendar_dates.txt')
    zipf.write('routes.txt')                    
    zipf.write('trips.txt')
    zipf.write('shapes.txt')
    zipf.write('stop_times.txt')

print('GTFS saved to', OUTPUT)
