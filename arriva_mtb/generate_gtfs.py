from pathlib import Path
from datetime import date, datetime, timedelta
import time
import shutil
import tempfile
import json
import zipfile
import tqdm
import requests
import pandas as pd
import csv
import hashlib
import os
import pytz
import concurrent.futures


OUTPUT = Path('arriva_mp_gtfs.zip').absolute()
CACHE_DIR = tempfile.TemporaryDirectory()
os.chdir(CACHE_DIR.name)

class Route:
    def __init__(self, route_id, route_short_name, route_long_name, route_type, route_color, route_text_color, agency_id='MPK'):
        self.route_id = route_id
        self.route_short_name = route_short_name
        self.route_long_name = route_long_name
        self.route_type = route_type
        self.route_color = route_color
        self.route_text_color = route_text_color
        self.agency_id = agency_id

    def __repr__(self):
        return f'Route({self.route_id}, {self.route_short_name}, {self.route_long_name}, {self.route_type}, {self.route_color}, {self.route_text_color})'

    def __str__(self):
        return f'{self.route_id} - {self.route_short_name} - {self.route_long_name}'

class Trip:
    def __init__(self, route_id, service_id, trip_id, trip_headsign):
        self.route_id = route_id
        self.shape_id = route_id
        self.service_id = service_id
        self.trip_id = trip_id
        self.trip_headsign = trip_headsign

    def __repr__(self):
        return f'Trip({self.route_id}, {self.service_id}, {self.trip_id}, {self.trip_headsign})'

    def __str__(self):
        return f'{self.trip_id} - {self.trip_headsign}'

    def set_start_time(self, start_time):
        self.start_time = start_time

    def set_end_time(self, end_time):
        self.end_time = end_time

class StopTime:
    def __init__(self, trip_id, arrival_time, departure_time, stop_id, stop_sequence):
        self.trip_id = trip_id
        self.arrival_time = arrival_time
        self.departure_time = departure_time
        self.stop_id = stop_id
        self.stop_sequence = stop_sequence
        self.timepoint = 1

    def __repr__(self):
        return f'StopTime({self.trip_id}, {self.arrival_time}, {self.departure_time}, {self.stop_id}, {self.stop_sequence})'

    def __str__(self):
        return f'{self.trip_id} - {self.stop_id} - {self.arrival_time}'

class Shape:
    def __init__(self, shape_id, shape_pt_lat, shape_pt_lon, shape_pt_sequence):
        self.shape_id = shape_id
        self.shape_pt_lat = shape_pt_lat
        self.shape_pt_lon = shape_pt_lon
        self.shape_pt_sequence = shape_pt_sequence

    def __repr__(self):
        return f'Shape({self.shape_id}, {self.shape_pt_lat}, {self.shape_pt_lon}, {self.shape_pt_sequence})'

    def __str__(self):
        return f'{self.shape_id} - {self.shape_pt_lat} - {self.shape_pt_lon}'

def get(url, headers=None, **kwargs):
    # Simple retry logic
    while True:
        try:
            return requests.get(url, headers=headers, **kwargs)
        except Exception:
            print('Connection error, retrying...')
            time.sleep(1)

day_start = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
day_plus_14 = day_start + timedelta(days=14)

# Create the agency
agency_df = pd.DataFrame([{
    'agency_id': 'ARRIVA',
    'agency_name': 'Arriva Mestni promet',
    'agency_url': 'https://arriva.si/',
    'agency_timezone': 'Europe/Ljubljana',
}])

API_BASE_URL = 'https://arriva-mtb-prod.lit-transit.com/mtbp/'
API_KEY = 'ApiKey bTl7H90FCtXPhsVTcxvyKWwIB8x4jc4J'

time.sleep(15 * 60)

# Get the base data
base_data = get(API_BASE_URL + 'service/ui/master-data/sl-si', headers={'Authorization': API_KEY}).json()

stops_list = []

# Create the stops
for _, stop in base_data['stops'].items():
    stops_list.append({
        'stop_id': stop['id'],
        'stop_code': stop['code'],
        'stop_name': stop['name'],
        'stop_lat': stop['latitude'],
        'stop_lon': stop['longitude'],
    })

def kranj_route_handler(route_short_name):
    return str(int(route_short_name[1:3]))

def jesenice_route_handler(route_short_name):
    return str(int(route_short_name[-1]))

def primorska_route_handler(route_short_name):
    name = route_short_name[1:3]
    if int(name) == 0:
        name == '01'
    int_name = int(name)
    if int_name < 20:
        if int_name == 2 and int(route_short_name[-1]) > 1:
            return '2A'
        
        return str(int_name)
    else:
        if int_name > 24:
            return '2'
        else:
            return '1'

stops_to_fetch = [
    # (stop_id, only_mp, mandatory_prefix, route_handler_func)
    # Primorska
    ('Arriva SLO:13527', True, '', primorska_route_handler),
    ('Arriva SLO:13513', True, '', primorska_route_handler),
    ('Arriva SLO:16005', True, '', primorska_route_handler),
    ('Arriva SLO:9505', True, '', primorska_route_handler),
    ('Arriva SLO:15202', True, '', primorska_route_handler),
    ('Arriva SLO:14483', True, '', primorska_route_handler),
    ('Arriva SLO:15836', True, '', primorska_route_handler),
    ('Arriva SLO:15806', True, '', primorska_route_handler),
    ('Arriva SLO:15319', True, '', primorska_route_handler),
    ('Arriva SLO:14249', True, '', primorska_route_handler),
    ('Arriva SLO:14483', True, '', primorska_route_handler),
    # Kranj
    ('Arriva SLO:9898', False, '4', kranj_route_handler),
    ('Arriva SLO:10764', False, '4', kranj_route_handler),
    ('Arriva SLO:12409', False, '4', kranj_route_handler),
    ('Arriva SLO:12426', False, '4', kranj_route_handler),
    ('Arriva SLO:12429', False, '4', kranj_route_handler),
    ('Arriva SLO:12433', False, '4', kranj_route_handler),
    # Ptuj
    ('Arriva SLO:15895', False, '', lambda x: x),
    ('Arriva SLO:15850', False, '', lambda x: x),
    ('Arriva SLO:15851', False, '', lambda x: x),
    ('Arriva SLO:14577', False, '', lambda x: x),
    ('Arriva SLO:11081', False, '', lambda x: x),
    ('Arriva SLO:14037', False, '', lambda x: x),
    ('Arriva SLO:15887', False, '', lambda x: x),
    ('Arriva SLO:14003', False, '', lambda x: x),
    # Jesenice
    ('Arriva SLO:13117', False, '699', jesenice_route_handler),
    # Škofja Loka
    ('Arriva SLO:10112', False, '', lambda x: '1'),
]

route_set = set()
route_map_handlers = {}
trip_id_set = set()
pattern_set = set()
trip_active_dates = {}

stop_times_list = []
trips_list = []
routes_list = []
shapes_list = []

utc = pytz.UTC
ljubljana_tz = pytz.timezone('Europe/Ljubljana')

dates_to_fetch = list(pd.date_range(day_start, day_plus_14, freq='D'))

########################################
# PHASE 1: FETCH TRIP METADATA IN PARALLEL
########################################

def fetch_stop_data(args):
    stop_id, only_mp, mandatory_prefix, route_handler_func, date_, hour_offset = args
    timestamp = int(date_.timestamp())
    url = API_BASE_URL + f'service/ui/eta/stop/{stop_id}/{timestamp + hour_offset * 3600}/1000?locale=sl-si'
    resp = get(url, headers={'Authorization': API_KEY}).json()
    results = []
    for route_id, trips in resp.items():
        for trip in trips:
            if 'MP' not in trip['tripId'] and only_mp:
                continue
            if mandatory_prefix and not trip['routeId'].split(':')[1].startswith(mandatory_prefix):
                continue
            # Collect trip info
            results.append((trip, date_))
    return results

# Prepare tasks
fetch_args = []
for stop_id, only_mp, mandatory_prefix, route_handler_func in stops_to_fetch:
    for date_ in dates_to_fetch:
        for i in range(0, 24, 3):
            fetch_args.append((stop_id, only_mp, mandatory_prefix, route_handler_func, date_, i))

trip_src_list = []

print("Fetching trip metadata from stops in parallel...")
with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
    futures = [executor.submit(fetch_stop_data, arg) for arg in fetch_args]
    for f in tqdm.tqdm(concurrent.futures.as_completed(futures), total=len(futures)):
        result = f.result()
        for trip, date_ in result:
            route_set.add(trip['routeId'])
            route_map_handlers[trip['routeId']] = stops_to_fetch[0][3]  # Will be overwritten correctly later, we fix this below
            pattern_set.add(trip['patternId'])
            if trip['tripId'] not in trip_id_set:
                trip_id_set.add(trip['tripId'])
                trip_src_list.append(trip)
                trip_active_dates[trip['tripId']] = set()
            trip_active_dates[trip['tripId']].add(date_.strftime('%Y%m%d'))

# To replicate original logic precisely, we can re-fetch all and check conditions again (inefficient but safe):
# Instead, we know each trip had 'routeId'. We also know that logic: route_map_handlers[trip['routeId']] = route_handler_func
# was set at the time the trip was encountered. Let's do a second pass over trip_src_list and stops_to_fetch.
for stop_id, only_mp, mandatory_prefix, r_func in stops_to_fetch:
    # Identify all trips from this stop that would have set the handler.
    # We must guess the condition used before:
    # The condition: "if 'MP' not in trip['tripId'] and only_mp: continue" and "if mandatory_prefix and not trip['routeId'].split(':')[1].startswith(mandatory_prefix): continue"
    for trip in trip_src_list:
        if (only_mp and 'MP' not in trip['tripId']):
            continue
        if (mandatory_prefix and not trip['routeId'].split(':')[1].startswith(mandatory_prefix)):
            continue
        # If we got here, the route_handler_func would have been set.
        # Only set if not already set to avoid overwriting.
        if trip['routeId'] not in route_map_handlers:
            route_map_handlers[trip['routeId']] = r_func


########################################
# PHASE 2: FETCH TRIP STOP TIMES IN PARALLEL
########################################

def fetch_trip_stop_times(args):
    trip, active_dates = args
    # We'll return the processed stop_times from here
    trip_stop_times = []
    # We fetch stop times once per date. According to original logic, we do it for each date in active_dates?
    # Original code fetched stop times only once at the time a trip was first seen on a given date.
    # But now we must replicate that logic. Actually the original code got the stop times for each trip
    # and date in the moment it discovered it. We'll just fetch for each date in active_dates and choose one that works.
    # If multiple dates produce different stop times, we can store them all.
    # However, original logic stored stop times only once per discovered date. Let's do the same here.

    # To be safe, we will fetch stop times once per date. If differences occur, we'll include them (since they represent each day's schedule).
    # Original code snippet: It fetched "trip_data" right after discovering a trip on a given date.
    # We'll do exactly the same: fetch them for each date in active_dates.

    for d_ in active_dates:
        url = API_BASE_URL + f'service/api/v1/otp/trips/{trip["tripId"]}/stop-times?locale=sl-si&date={d_}'
        trip_data = get(url, headers={'Authorization': API_KEY}).json()

        passes_midnight = False
        previous_stop_time = '00:00:00'
        for stop_time in trip_data['stopTimeList']:
            # Parse times
            arr_iso = stop_time['scheduledArrival']
            dep_iso = stop_time['scheduledDeparture']

            # Extract tz offset
            # Format: 2024-12-11T05:00:00+0100
            arr_dt = datetime.fromisoformat(arr_iso.split('+')[0])
            dep_dt = datetime.fromisoformat(dep_iso.split('+')[0])

            arr_offset = int(arr_iso.split('+')[1][0:2]) * 60 + int(arr_iso.split('+')[1][2:4])
            dep_offset = int(dep_iso.split('+')[1][0:2]) * 60 + int(dep_iso.split('+')[1][2:4])

            arr_dt = arr_dt - timedelta(minutes=arr_offset)
            dep_dt = dep_dt - timedelta(minutes=dep_offset)

            arr_str = arr_dt.astimezone(ljubljana_tz).strftime('%H:%M:%S')
            dep_str = dep_dt.astimezone(ljubljana_tz).strftime('%H:%M:%S')

            # Handle midnight passing
            if int(arr_str.split(':')[0]) < int(previous_stop_time.split(':')[0]):
                passes_midnight = True
                arr_str = f'{int(arr_str.split(":")[0])+24}:{arr_str.split(":")[1]}:{arr_str.split(":")[2]}'
                dep_str = f'{int(dep_str.split(":")[0])+24}:{dep_str.split(":")[1]}:{dep_str.split(":")[2]}'
            elif int(dep_str.split(':')[0]) < int(arr_str.split(':')[0]):
                passes_midnight = True
                dep_str = f'{int(dep_str.split(":")[0])+24}:{dep_str.split(":")[1]}:{dep_str.split(":")[2]}'
            elif passes_midnight:
                # Already passed midnight, increment hours by 24
                arr_str = f'{int(arr_str.split(":")[0])+24}:{arr_str.split(":")[1]}:{arr_str.split(":")[2]}'
                dep_str = f'{int(dep_str.split(":")[0])+24}:{dep_str.split(":")[1]}:{dep_str.split(":")[2]}'

            trip_stop_times.append({
                'trip_id': trip['tripId'],
                'arrival_time': arr_str,
                'departure_time': dep_str,
                'stop_id': stop_time['stopId'],
                'stop_sequence': stop_time['stopIndex'] + 1,
            })
            previous_stop_time = arr_str  # Update for next iteration

    return trip_stop_times

print("Fetching trip stop times in parallel...")
args_list = [(t, trip_active_dates[t['tripId']]) for t in trip_src_list]

all_trip_stop_times = []
with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
    futures = [executor.submit(fetch_trip_stop_times, arg) for arg in args_list]
    for f in tqdm.tqdm(concurrent.futures.as_completed(futures), total=len(futures)):
        res = f.result()
        all_trip_stop_times.extend(res)

# Now we have all stop times
stop_times_list = all_trip_stop_times

def hash_dates(dates):
    dates = sorted(dates)
    return hashlib.md5('|'.join(dates).encode()).hexdigest()

for trip in trip_src_list:
    trips_list.append({
        'route_id': trip['routeId'],
        'service_id': hash_dates(trip_active_dates[trip['tripId']]),
        'trip_id': trip['tripId'],
        'trip_headsign': trip['headSign'],
        'shape_id': trip['patternId'],
    })

def dark_or_bright(color):
    color = color.lstrip('#')
    r, g, b = int(color[:2], 16), int(color[2:4], 16), int(color[4:], 16)
    return 'FFFFFF' if r * 0.299 + g * 0.587 + b * 0.114 > 160 else '000000'

for route in base_data['routes'].values():
    if route['id'] not in route_set:
        continue
    routes_list.append({
        'agency_id': 'ARRIVA',
        'route_id': route['id'],
        'route_short_name': route['routeGroupCode'] if len(route['routeGroupCode']) > 0 else route_map_handlers[route['id']](route['shortName']),
        'route_long_name': route['longName'] or '',
        'route_type': 3,
        'route_color': route['color'].lstrip('#'),
        'route_text_color': dark_or_bright(route['color']),
    })

def decode_polyline(polyline_str):
    index, lat, lon = 0, 0, 0
    coordinates = []
    while index < len(polyline_str):
        shift, result = 0, 0
        while True:
            byte = ord(polyline_str[index]) - 63
            index += 1
            result |= (byte & 0x1F) << shift
            shift += 5
            if byte < 0x20:
                break
        lat += ~(result >> 1) if result & 1 else result >> 1
        shift, result = 0, 0
        while True:
            byte = ord(polyline_str[index]) - 63
            index += 1
            result |= (byte & 0x1F) << shift
            shift += 5
            if byte < 0x20:
                break
        lon += ~(result >> 1) if result & 1 else result >> 1
        coordinates.append((lat / 1e5, lon / 1e5))
    return coordinates

for pattern in base_data['routePatterns'].values():
    if pattern['id'] not in pattern_set:
        continue
    points = decode_polyline(pattern['geometry']['points'])
    for i, point in enumerate(points):
        shapes_list.append({
            'shape_id': pattern['id'],
            'shape_pt_lat': point[0],
            'shape_pt_lon': point[1],
            'shape_pt_sequence': i + 1,
        })

calendar_list = [{
    'service_id': hash_dates(trip_active_dates[trip['tripId']]),
    'monday': 0,
    'tuesday': 0,
    'wednesday': 0,
    'thursday': 0,
    'friday': 0,
    'saturday': 0,
    'sunday': 0,
    'start_date': day_start.strftime('%Y%m%d'),
    'end_date': day_plus_14.strftime('%Y%m%d'),
} for trip in trip_src_list]

calendar_dates_list = []
for trip_id, dates in trip_active_dates.items():
    hash_dates_str = hash_dates(trip_active_dates[trip_id])
    for date_ in dates:
        calendar_dates_list.append({
            'service_id': hash_dates_str,
            'date': date_,
            'exception_type': 1,
        })

# Create dataframes
trips_df = pd.DataFrame(trips_list).drop_duplicates()
stop_times_df = pd.DataFrame(stop_times_list).drop_duplicates()
shapes_df = pd.DataFrame(shapes_list).drop_duplicates()
routes_df = pd.DataFrame(routes_list).drop_duplicates()
stops_df = pd.DataFrame(stops_list).drop_duplicates()
calendar_df = pd.DataFrame(calendar_list).drop_duplicates()
calendar_dates_df = pd.DataFrame(calendar_dates_list).drop_duplicates()

stop_map = {stop['stop_id']: stop['stop_name'] for _, stop in stops_df.iterrows()}

def calc_headsign(trip_id):
    stops_this_trip = stop_times_df[stop_times_df['trip_id'] == trip_id].sort_values('stop_sequence')
    if len(stops_this_trip) == 0:
        return ''
    first_stop = stop_map[stops_this_trip.iloc[0]['stop_id']]
    last_stop = stop_map[stops_this_trip.iloc[-1]['stop_id']]
    if first_stop == last_stop and len(stops_this_trip) > 2:
        mid_stop = stop_map[stops_this_trip.iloc[len(stops_this_trip)//2]['stop_id']]
        return f'{first_stop} - {mid_stop} - {last_stop}'
    return f'{first_stop} - {last_stop}'

trips_df['trip_headsign'] = trips_df['trip_id'].apply(calc_headsign)

# Remove unused stops
stop_ids = set(stop_times_df['stop_id'])
stops_df = stops_df[stops_df['stop_id'].isin(stop_ids)]

# Save GTFS files
trips_df.to_csv('trips.txt', index=False)
print('trips.txt saved')
stop_times_df.to_csv('stop_times.txt', index=False)
print('stop_times.txt saved')
shapes_df.to_csv('shapes.txt', index=False)
print('shapes.txt saved')
routes_df.to_csv('routes.txt', index=False)
print('routes.txt saved')
stops_df.to_csv('stops.txt', index=False)
print('stops.txt saved')
calendar_df.to_csv('calendar.txt', index=False)
print('calendar.txt saved')
calendar_dates_df.to_csv('calendar_dates.txt', index=False)
print('calendar_dates.txt saved')
agency_df.to_csv('agency.txt', index=False)
print('agency.txt saved')

# Remove 'Arriva SLO:' from IDs
for file in ['trips.txt', 'stop_times.txt', 'shapes.txt', 'routes.txt', 'stops.txt']:
    with open(file, 'r') as f:
        data = f.read()
    data = data.replace('Arriva SLO:', '')
    with open(file, 'w') as f:
        f.write(data)

# Write the GTFS into a zip file
with zipfile.ZipFile(OUTPUT, 'w') as zipf:
    zipf.write('agency.txt')
    zipf.write('stops.txt')
    zipf.write('calendar.txt')
    zipf.write('calendar_dates.txt')
    zipf.write('routes.txt')
    zipf.write('trips.txt')
    zipf.write('shapes.txt')
    zipf.write('stop_times.txt')

print('GTFS saved to', OUTPUT)
