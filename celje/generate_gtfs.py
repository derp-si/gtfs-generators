import datetime
import requests
import base64
import urllib.parse
import construct
import io
import hashlib
import re
import pandas as pd
import shapely.wkt
import zipfile
import tempfile
import sys
import os
from pathlib import Path


HERE = Path(__file__).parent.parent.absolute()
CACHE_DIR = tempfile.TemporaryDirectory()
os.chdir(CACHE_DIR.name)

if os.environ.get('CELJE_CB_API_KEY') or sys.argv.get(1):
    print('Context Broker API key is set')
else:
    print('Context Broker API key is not set - aborting!')

class Stop():
    def __init__(self, id, name, lat, lon):
        self.stop_id = str(id)
        self.stop_name = name
        self.stop_lat = lat
        self.stop_lon = lon
        self.stop_code = ""

POINT_STRUCT = construct.Struct(
    'srid' / construct.Int32ul,
    'version' / construct.Int8ul,
    'serialization' / construct.Int8ul,
    'lon' / construct.Float64l,
    'lat' / construct.Float64l
)

LINE_STRING_POINT_STRUCT = construct.Struct(
    'lon' / construct.Float64l,
    'lat' / construct.Float64l
)

LINE_STRING_STRUCT = construct.Struct(
    'srid' / construct.Int32ul,
    'version' / construct.Int8ul,
    'serialization' / construct.Int8ul, 
    'points_count' / construct.Int32ul, 
    'points' / construct.GreedyRange(LINE_STRING_POINT_STRUCT)
)   

class Agency():
    def __init__(self, id, name, url):
        self.agency_id = str(id)
        self.agency_name = name
        self.agency_url = url
        self.agency_timezone = 'Europe/Ljubljana'

class Route():
    def __init__(self, id, name):
        self.route_id = str(id)
        self.route_long_name = name

class Calendar():
    def __init__(self, id, start_date, end_date, default_vector):
        self.service_id = str(id)
        self.start_date = start_date
        self.end_date = end_date
        self.monday = default_vector
        self.tuesday = default_vector
        self.wednesday = default_vector
        self.thursday = default_vector
        self.friday = default_vector
        self.saturday = default_vector
        self.sunday = default_vector

    def set_day(self, day, value):
        if day == 'Monday':
            self.monday = value
        elif day == 'Tuesday':
            self.tuesday = value
        elif day == 'Wednesday':
            self.wednesday = value
        elif day == 'Thursday':
            self.thursday = value
        elif day == 'Friday':
            self.friday = value
        elif day == 'Saturday':
            self.saturday = value
        elif day == 'Sunday':
            self.sunday = value

class CalendarDate():
    def __init__(self, service_id, date, exception_type):
        self.service_id = service_id
        self.date = date
        self.exception_type = exception_type

class Route():
    def __init__(self, id, route_short_name, route_long_name):
        self.route_id = str(id)
        self.agency_id = '1'
        self.route_type = '3'
        self.route_short_name = route_short_name
        self.route_long_name = route_long_name

class Trip():
    def __init__(self, id, route_id, service_id, trip_headsign):
        self.route_id = route_id
        self.service_id = service_id
        self.trip_id = str(id)
        self.trip_headsign = trip_headsign
        self.shape_id = ''
    
    def set_shape_id(self, shape_id):
        self.shape_id = shape_id

class Shape():
    def __init__(self, id, shape_pt_lat, shape_pt_lon, shape_pt_sequence):
        self.shape_id = str(id)
        self.shape_pt_lat = shape_pt_lat
        self.shape_pt_lon = shape_pt_lon
        self.shape_pt_sequence = shape_pt_sequence

class StopTime():
    def __init__(self, trip_id, arrival_time, departure_time, stop_id, stop_sequence, timepoint=0):
        self.trip_id = trip_id
        self.arrival_time = arrival_time
        self.departure_time = departure_time
        self.stop_id = stop_id
        self.stop_sequence = stop_sequence
        self.timepoint = timepoint

BASE_URL = 'http://centralka-proxy.derp.si/v2/entities?type='

def simplify_response(response: dict) -> dict:
    """Converts the context broker response to a simpler dictionary."""
    new_response = []
    for entity in response:
        new_entity = {}
        for key, value in entity.items():
            if key == 'id':
                new_entity['id'] = value
            elif key == 'type':
                new_entity['type'] = value
            else:
                new_entity[key] = value['value'] if value else value
        new_response.append(new_entity)
    return new_response

def fetch_dataset(type: str) -> dict:
    api_key = os.environ.get('CELJE_CB_API_KEY') or sys.argv[1]
    data = []
    offset = 0
    while True:
        url = BASE_URL + type + '&limit=100' + '&offset=' + str(offset)
        response = requests.get(url, verify=False, headers={'Fiware-Service': 'moc_afc', 'X-Auth-Token': api_key})
        response_json = response.json()
        if len(response_json) == 0:
            break
        data += simplify_response(response_json)
        offset += 100
    print(f'Data for {type} fetched, total records: {len(data)}')
    return data

if __name__ == '__main__':
    print('Fetching TSStation dataset...')
    stations = fetch_dataset('TSStation')
    station_dict = {station['Id']: station for station in stations}
    
    print('Fetching TSStationPoint dataset...')
    station_points = fetch_dataset('TSStationPoint')

    agency_csv = [Agency(
        1,
        'CELEBUS/Nomago',
        'https://www.nomago.si/'
    )]

    stops_csv = []
    stop_point_to_name_map = {}

    for station_point in station_points:
        station = station_dict[station_point['StationId']]
        # Geometry is in the MS SQL spatial format, which is a binary format.
        # We need to decode it to get the latitude and longitude.
        geometry = POINT_STRUCT.parse(base64.b64decode(urllib.parse.unquote(station_point['Geometry'])))

        stops_csv.append(Stop(str(station_point['Id']), station['Name'], geometry['lat'], geometry['lon']))
        stop_point_to_name_map[str(station_point['Id'])] = station['Name']

    print('Fetching LitTranzitStation dataset...')
    lit_stations = fetch_dataset('LitTranzitStation')
    lit_stops = []
    for lit_station in lit_stations:
        lit_stops.append(Stop(lit_station['LitTranzitStationId'], lit_station['StopName'], lit_station['Latitude'], lit_station['Longitude']))

    # Match the stops based on latitude and longitude
    for stop in stops_csv:
        for lit_stop in lit_stops:
            if abs(stop.stop_lat - lit_stop.stop_lat) < 0.000001 and abs(stop.stop_lon - lit_stop.stop_lon) < 0.000001:
                stop.stop_code = lit_stop.stop_id
                break

    print('Fetching TSRegime dataset...')
    regimes = fetch_dataset('TSRegime')
    
    print('Fetching TSRegimeInterval dataset...')
    regime_intervals = fetch_dataset('TSRegimeInterval')
    
    print('Fetching TSRegimeTerm dataset...')
    regime_terms = fetch_dataset('TSRegimeTerm')
    
    print('Fetching TSWorkVector dataset...')
    regime_vectors = fetch_dataset('TSWorkVector')

    print('Fetching TSTimetable dataset...')
    timetables = fetch_dataset('TSTimetable')

    # We will create a map that ignores the trip if it's timetable has a version set
    timetables_map = {
        timetable['Id']: type(timetable['Version']) is int for timetable in timetables
    }

    calendars_csv = []

    # set the start day as yesterday, and the end day 365 days from now
    start_date = datetime.datetime.now() - datetime.timedelta(days=1)
    end_date = datetime.datetime.now() + datetime.timedelta(days=365)

    for regime in regimes:
        if regime['UnderConsideration']:
            continue
        calendars_csv.append(Calendar(regime['Id'], start_date.strftime('%Y%m%d'), end_date.strftime('%Y%m%d'), '0'))

    calendar_dates_csv = []
    
    for regime_vector in regime_vectors:
        if regime_vector['Year'] not in [start_date.year, end_date.year]:
            continue
        for index, vector in enumerate(regime_vector['WorkVector']):
            gtfs_vector = '1' if vector == '1' else '2'
            date = datetime.datetime(regime_vector['Year'], 1, 1) + datetime.timedelta(days=index)
            calendar_dates_csv.append(CalendarDate(regime_vector['RegimeId'], date.strftime('%Y%m%d'), gtfs_vector))
            b = 0

    print('Fetching TSRoute dataset...')
    routes = fetch_dataset('TSRoute')
    
    print('Fetching LitTranzitLine dataset...')
    lit_routes = fetch_dataset('LitTranzitLine')
    
    print('Fetching LitTranzitLineStationLiveData dataset...')
    lit_live_data = fetch_dataset('LitTranzitLineStationLiveData')
    
    print('Fetching TSTrip dataset...')
    trips = fetch_dataset('TSTrip')
    
    print('Fetching TSStatusCL dataset...')
    status = fetch_dataset('TSStatusCL')
    
    print('Fetching TSTripRoute dataset...')
    trips_route = fetch_dataset('TSTripRoute')
    trip_id_to_route = {trip['TripId']: trip['RouteId'] for trip in trips_route}
    trips_in_trip_csv = []

    routes_csv = []

    lit_trip_id_to_route_id = {lit_live_data['LineJourneyId']: lit_live_data['LitTranzitLineId'] for lit_live_data in lit_live_data}

    trips_csv = []

    native_route_id_to_lit_id = {}

    for trip in trips:
        if trip['StatusId'] not in [4, 7, 13]:  # 4 - registriran, 7 - nov v potrjevanju, 11 - potrjen
            continue
        if not trip_id_to_route[trip['Id']]:
            continue
        if timetables_map[trip['TimetableId']]:
            continue
        trips_csv.append(Trip(
            trip['Id'],
            trip_id_to_route[trip['Id']],
            trip['RegimeId'],
            ''
        ))
        if lit_trip_id_to_route_id.get(str(trip['Id'])):
            native_route_id_to_lit_id[trip_id_to_route[trip['Id']]] = lit_trip_id_to_route_id[str(trip['Id'])]
        trips_in_trip_csv.append(trip['Id'])

    print('Writing shapes from the WKT...')
    shapes_csv = [] 

    for lit_route in lit_routes:
        shape = shapely.wkt.loads(urllib.parse.unquote(lit_route['Geometry']))
        for i, point in enumerate(shape.coords):
            shapes_csv.append(Shape(lit_route['Id'], point[1], point[0], i))

    route_ids_in_csv = []

    route_colors = {
        '1': ('e40e44', 'ffffff'),
        '1A': ('e40e44', 'ffffff'),
        '2': ('aac34e', 'ffffff'),  
        '3': ('048cc6', 'ffffff'),  
        '4': ('fb5c94', 'ffffff'),  
        '5': ('ff9a56', 'ffffff'),
        '5A': ('a15cad', 'ffffff'),
        '6': ('fbea38', '000000 '),
        '7': ('178048', 'ffffff'),
        '8': ('293e7d', 'ffffff'),
        '9A': ('000000', 'ffffff'),
        '9B': ('000000', 'ffffff'), 
    }

    for route in routes:
        lit_route = [lit_route for lit_route in lit_routes if lit_route['Id'] == native_route_id_to_lit_id.get(route['Id'])]
        if len(lit_route) > 0:
            # Lit routes are named as NAME (Linija SHORT_NAME), so we need to extract the name and short name
            name = urllib.parse.unquote(lit_route[0]['Description'])
            short_name = re.search(r'\((.*?)\)', name).group(1).split(' ')[1]
            long_name = ' - '.join(name.split(' (')[0].split('-'))
            # Update all trips on this route with the geometry
            for trip in trips_csv:
                if str(trip.route_id) == str(route['Id']):
                    trip.trip_headsign = long_name
                    trip.set_shape_id(str(lit_route[0]['Id']))
            route_ids_in_csv.append(route['Id'])
        else:
            # Might as well skip this route if it doesn't have a lit route
            continue
        routes_csv.append(Route(route['Id'], short_name, long_name))
        # set the route color based on the route short name
        if route_colors.get(short_name):
            route_color, route_text_color = route_colors[short_name]
            routes_csv[-1].route_color = route_color
            routes_csv[-1].route_text_color = route_text_color
        else:
            routes_csv[-1].route_color = '000000'
            routes_csv[-1].route_text_color = 'ffffff'

    # Remove trips that are not in the routes
    trips_csv = [trip for trip in trips_csv if trip.route_id in route_ids_in_csv]
    trips_in_trip_csv = set([trip.trip_id for trip in trips_csv])

    print('Fetching TSTripDescription dataset...')
    trip_descriptions = fetch_dataset('TSTripDescription')

    stop_times_csv = []

    stop_ids_in_stop_times = set()

    print('Generating stop times...', end='')

    for trip_description in trip_descriptions:
        if str(trip_description['TripId']) not in trips_in_trip_csv:
            continue
        arrival_time = trip_description['TimeOfArrival'] or trip_description['TimeOfDeparture']
        departure_time = trip_description['TimeOfDeparture'] or trip_description['TimeOfArrival']
        stop_times_csv.append(StopTime(trip_description['TripId'], 
                                       arrival_time, departure_time, trip_description['StationPointId'], trip_description['SequentialStationNumber'],
                                       trip_description['StopOnStation']))
        stop_ids_in_stop_times.add(trip_description['StationPointId'])

        b = 0

    print('DONE!')

    stop_ids_in_stop_times = set([str(stop_id) for stop_id in stop_ids_in_stop_times])
    # Remove stops that are not in the stop times
    stops_csv = [stop for stop in stops_csv if stop.stop_id in stop_ids_in_stop_times]

    # Calculate the headsigns for the trips

    print('Calculating trip headsigns...', end='')

    for trip in trips_csv:
        if trip.trip_headsign:
            continue
        trip_stops = [stop_time for stop_time in stop_times_csv if str(stop_time.trip_id) == trip.trip_id]
        if len(trip_stops) == 0:
            continue
        trip_stops = sorted(trip_stops, key=lambda x: x.stop_sequence)
        first_stop = stop_point_to_name_map[str(trip_stops[0].stop_id)]
        last_stop = stop_point_to_name_map[str(trip_stops[-1].stop_id)]
        trip.trip_headsign = f'{first_stop} - {last_stop}'.upper()

    print('DONE!')

    print('Exporting data to CSV files...')
    agency_df = pd.DataFrame([vars(agency) for agency in agency_csv])
    agency_df.to_csv('agency.txt', index=False)
    print('agency.txt created')

    stops_df = pd.DataFrame([vars(stop) for stop in stops_csv])
    stops_df.to_csv('stops.txt', index=False)
    print('stops.txt created')

    calendars_df = pd.DataFrame([vars(calendar) for calendar in calendars_csv])
    calendars_df.to_csv('calendar.txt', index=False)
    print('calendar.txt created')

    calendar_dates_df = pd.DataFrame([vars(calendar_date) for calendar_date in calendar_dates_csv])
    calendar_dates_df.to_csv('calendar_dates.txt', index=False)
    print('calendar_dates.txt created')

    routes_df = pd.DataFrame([vars(route) for route in routes_csv])
    routes_df.to_csv('routes.txt', index=False)
    print('routes.txt created')

    trips_df = pd.DataFrame([vars(trip) for trip in trips_csv])
    trips_df.to_csv('trips.txt', index=False)
    print('trips.txt created')

    stop_times_df = pd.DataFrame([vars(stop_time) for stop_time in stop_times_csv])
    stop_times_df.to_csv('stop_times.txt', index=False)
    print('stop_times.txt created')
    
    shapes_df = pd.DataFrame([vars(shape) for shape in shapes_csv])
    shapes_df.to_csv('shapes.txt', index=False)
    print('shapes.txt created')

    print('Zipping the files...')
    with zipfile.ZipFile(HERE / 'celje.zip', 'w') as zipf:
        zipf.write('agency.txt')
        zipf.write('stops.txt')
        zipf.write('calendar.txt')
        zipf.write('calendar_dates.txt')
        zipf.write('routes.txt')                    
        zipf.write('trips.txt')
        zipf.write('shapes.txt')
        zipf.write('stop_times.txt')

    print('DONE!')
    print(' Saved as celje.zip')
