# GTFS generators and fixers

## IJPP

**Permalink:** https://gitlab.com/derp-si/gtfs-generators/-/jobs/artifacts/main/raw/ijpp_gtfs.zip?job=IJPP

**Permalink for single agency:** https://gitlab.com/derp-si/gtfs-generators/-/jobs/artifacts/main/raw/ijpp_gtfs_tc.zip?job=IJPP

**APMS only:** https://gitlab.com/derp-si/gtfs-generators/-/jobs/artifacts/main/raw/apms_gtfs.zip?job=IJPP

Downloads the official GTFS, fixes route names, adds route colors and trip headsigns.    
It also creates a single-agency version (for TransitClock) and one with only AP Murska Sobota routes.

### NeTEx converter to GTFS (experimental)

**Permalink:** https://gitlab.com/derp-si/gtfs-generators/-/jobs/artifacts/main/raw/ijpp_netex_gtfs.zip?job=netex_to_gtfs_ijpp

**Logs:** https://gitlab.com/derp-si/gtfs-generators/-/jobs/artifacts/main/raw/stop_times_with_arrival_before_previous_departure_time.txt?job=netex_to_gtfs_ijpp

---

#### Fix (like the official GTFS)

**Permalink:** https://gitlab.com/derp-si/gtfs-generators/-/jobs/artifacts/main/raw/ijpp_gtfs.zip?job=process_netex_gtfs

**Permalink for single agency:** https://gitlab.com/derp-si/gtfs-generators/-/jobs/artifacts/main/raw/ijpp_gtfs_tc.zip?job=process_netex_gtfs

**APMS only:** https://gitlab.com/derp-si/gtfs-generators/-/jobs/artifacts/main/raw/apms_gtfs.zip?job=process_netex_gtfs

## LPP

**Permalink:** https://gitlab.com/derp-si/gtfs-generators/-/jobs/artifacts/main/raw/lpp_gtfs.zip?job=LPP

Downloads the official GTFS and adds the shape file, adds colors to route and removes lines 321 and 95B.

## Marprom

**Permalink:** https://gitlab.com/derp-si/gtfs-generators/-/jobs/artifacts/main/raw/marprom_gtfs.zip?job=marprom

Generates a GTFS by getting data for the next workday, Saturday and Sunday from the "OBA" API.

**Permalink for fixed official GTFS:** https://gitlab.com/derp-si/gtfs-generators/-/jobs/artifacts/main/raw/marprom_gtfs.zip?job=marprom_official

Adds colors to routes.

## Celje

**Permalink:** https://gitlab.com/derp-si/gtfs-generators/-/jobs/artifacts/main/raw/celje.zip?job=CELEBUS

Generates a GTFS from the Context Broker responses.

## Koper

**Permalink:** https://gitlab.com/derp-si/gtfs-generators/-/jobs/artifacts/main/raw/mok_gtfs.zip?job=koper

Generates a GTFS from the PROMETWS API. Does not include shapes.

## Arriva MP

**Permalink:** https://gitlab.com/derp-si/gtfs-generators/-/jobs/artifacts/main/raw/arriva_mp_gtfs.zip?job=mtb