from itertools import chain
from pathlib import Path
import shutil
import tempfile
import json

import requests
import pandas as pd

##############
#   CONFIG   #
##############
CLEANUP = True
OUTPUT = Path('lpp_gtfs.zip')

if CLEANUP:
    CACHE_DIR = tempfile.TemporaryDirectory()
    ZIP_DIR = tempfile.TemporaryDirectory()
    CACHE_PATH = Path(CACHE_DIR.name)
    ZIP_PATH = Path(ZIP_DIR.name)
else:
    CACHE_PATH = Path('cache')
    ZIP_PATH = Path('lpp_gtfs')

ROUTES_CACHE = CACHE_PATH / 'lpp_routes_cache.json'
INPUT = CACHE_PATH / 'lpp_original_gtfs.zip'

#################
#   LOAD GTFS   #
#################

if not INPUT.exists():
    print('Downloading GTFS...', end="")
    resp = requests.get('https://lpp.ojpp.derp.si/api/gtfs/feed.zip')
    resp.raise_for_status()
    assert len(resp.content) > 100000, 'GTFS file is suspiciously small!'
    with INPUT.open('wb') as f:
        f.write(resp.content)
    print('DONE')

###################
#   LOAD ROUTES   #
###################

if ROUTES_CACHE and ROUTES_CACHE.exists():
    with ROUTES_CACHE.open() as f:
        routes_json = json.load(f)
else:
    print('Downloading route information...', end="")
    resp = requests.get('https://lpp.ojpp.derp.si/api/route/routes')
    resp.raise_for_status()
    routes_json = resp.json()
    assert routes_json['success']
    if ROUTES_CACHE:
        with ROUTES_CACHE.open('w') as f:
            json.dump(routes_json, f)
    print('DONE')


known_shapes = set()


print('Generating shapes...', end="")
shape_list = []
for route in routes_json['data']:
    shp = route.get('geojson_shape')
    if not shp:
        # Attempt to fetch shape from route
        resp = requests.get(f'https://lpp.ojpp.derp.si/api/route/routes?route-id={route["route_id"]}&trip-id={route["trip_id"]}&shape=1')
        if not resp.ok:
            print(f'Failed to fetch shape for route {route["route_id"]}')
            continue
        for entry in resp.json()['data']:
            shp = entry.get('geojson_shape', None)
            if not shp:
                print(f'Route {route["route_id"]} has no shape')
                continue
            else:
                if entry['trip_id'] == route['trip_id']:
                    print(f'Fetched shape for route {route["route_id"]} variant {entry["trip_id"]}')
                    route['geojson_shape'] = shp
                    break
                else:
                    print('Fetched shape for wrong trip (%s instead of %s)' % (entry['trip_id'], route['trip_id']))
                    continue

    shape_id = route['trip_id'].lower()
    known_shapes.add(shape_id)

    if not shp:
        print(f'Route {route["route_id"]} has no shape')
        continue

    coords = shp.get('coordinates', [])
    if shp.get('type') == 'MultiLineString':
        # TODO: handle MultiLineString better
        coords = chain(*coords)
    
    for i, coord in enumerate(coords):
        shape_list.append((
            shape_id,
            coord[1],
            coord[0],
            i + 1
        ))
shapes = pd.DataFrame(shape_list, columns=('shape_id','shape_pt_lat','shape_pt_lon','shape_pt_sequence',))
print("DONE")

# Save shapes to file
if ROUTES_CACHE:
    with ROUTES_CACHE.open('w') as f:
        json.dump(routes_json, f)

print('Extracting GTFS...')
shutil.unpack_archive(INPUT, ZIP_PATH)


print('Writing shapes')
shapes.to_csv(ZIP_PATH / 'shapes.txt', index=False)

print('Reading routes')
routes = pd.read_csv(ZIP_PATH / 'routes.txt')

print('    Removing routes with route_id 148E76C1-8F20-4FDF-9B83-A7C2D9B9739D or 4AEE50F1-B1B5-43EC-8A26-CBAA71FA5E59 (not public transport routes)')
routes = routes[~routes.route_id.isin(['148E76C1-8F20-4FDF-9B83-A7C2D9B9739D'.lower(), '4AEE50F1-B1B5-43EC-8A26-CBAA71FA5E59'.lower()])]

print('    Removing leading zeros from route short names')
routes.route_short_name = routes.route_short_name.str.lstrip('0')
routes.route_short_name = routes.route_short_name.str.replace('N0', 'N')

with open('lpp/colors.json') as f:
    colors = json.load(f)
    print('    Assigning colors to routes')
    colors_dict = {}
    for color in colors['body']['layers']:
        if color['type'] != 'proga':
            continue
        colors_dict[color['shortLabel']] = "{0:02x}{1:02x}{2:02x}".format(color['color']['r'], color['color']['g'], color['color']['b']).upper()
    
    for route in routes['route_short_name']:
        if route not in colors_dict:
            print(f'        Route {route} not found in colors.json, attempting to find a similar route')
            local_route = route[:-1] if not route[-1].isdigit() else route
            if local_route in colors_dict:
                print(f'            Found {local_route} in colors.json')
                colors_dict[route] = colors_dict[local_route]
            # try the other way round
            else:
                local_route = route[:1] if not route[0].isdigit() else route
                if local_route in colors_dict:
                    print(f'            Found {local_route} in colors.json')
                    colors_dict[route] = colors_dict[local_route]
                else:
                    # Check if there is a route with the same number but different letter, e.g. 1 and 1B
                    local_route = route
                    for key in colors_dict.keys():
                        if key[:-1] == route:
                            local_route = key
                            break
                    if local_route != route:
                        print(f'            Found {local_route} in colors.json')
                        colors_dict[route] = colors_dict[local_route]
                    else:
                        print(f'            Route {route} not found in colors.json')
                        colors_dict[route] = '000000'
        else:
            print(f'        Found {route} in colors.json')
        routes.loc[routes['route_short_name'] == route, 'route_color'] = colors_dict[route]

    routes['route_text_color'] = 'FFFFFF'

print('Writing routes')
routes.to_csv(ZIP_PATH / 'routes.txt', index=False)

print('Reading trips')
trips = pd.read_csv(ZIP_PATH / 'trips.txt')

# read stop times and remove trips with the route_id 148E76C1-8F20-4FDF-9B83-A7C2D9B9739D or 4AEE50F1-B1B5-43EC-8A26-CBAA71FA5E59, this is the train and UKC line which are not public transport
print('Reading stop times')
stop_times = pd.read_csv(ZIP_PATH / 'stop_times.txt')

affected_trip_ids = trips[trips.route_id.isin(['148E76C1-8F20-4FDF-9B83-A7C2D9B9739D'.lower(), '4AEE50F1-B1B5-43EC-8A26-CBAA71FA5E59'.lower()])]['trip_id']

print('    Removing stop times with route_id 148E76C1-8F20-4FDF-9B83-A7C2D9B9739D or 4AEE50F1-B1B5-43EC-8A26-CBAA71FA5E59 (not public transport routes)')
stop_times = stop_times[~stop_times.trip_id.isin(affected_trip_ids)]

print('Writing stop times')
stop_times.to_csv(ZIP_PATH / 'stop_times.txt', index=False)

# remove stops with no stop times
print('Reading stops')
stops = pd.read_csv(ZIP_PATH / 'stops.txt')

print('    Removing stops with no stop times')
stops = stops[stops.stop_id.isin(stop_times.stop_id)]

print('Writing stops')
stops.to_csv(ZIP_PATH / 'stops.txt', index=False)

print('    Removing trips with route_id 148E76C1-8F20-4FDF-9B83-A7C2D9B9739D or 4AEE50F1-B1B5-43EC-8A26-CBAA71FA5E59 (not public transport routes)')
trips = trips[~trips.route_id.isin(['148E76C1-8F20-4FDF-9B83-A7C2D9B9739D'.lower(), '4AEE50F1-B1B5-43EC-8A26-CBAA71FA5E59'.lower()])]

print('    Extracting subroutes')
subroute_ids = trips.trip_id.str.split('|', expand=True)[2]

#print('    Replacing routes with subroutes')
#trips['route_id'] = subroute_ids

print('    Adding shapes to trips')
shape_ids = subroute_ids.copy()
shape_ids[~shape_ids.isin(known_shapes)] = ''
trips['shape_id'] = shape_ids

print('Writing trips')
trips.to_csv(ZIP_PATH / 'trips.txt', index=False)

print('Compressing...', end="")
shutil.make_archive(str(OUTPUT).removesuffix('.zip'), 'zip', ZIP_PATH)
print('DONE')

if CLEANUP:
    CACHE_DIR.cleanup()
    ZIP_DIR.cleanup()


