from itertools import chain
from pathlib import Path
import shutil
import tempfile
import json
import tqdm
import requests
import pandas as pd
import struct

##############
#   CONFIG   #
##############
CLEANUP = True
OUTPUT = Path('ijpp_gtfs.zip')
NEW_OUTPUT = Path('ijpp_gtfs_tc.zip')
APMS_OUTPUT = Path('apms_gtfs.zip')

if CLEANUP:
    CACHE_DIR = tempfile.TemporaryDirectory()
    ZIP_DIR = tempfile.TemporaryDirectory()
    APMS_DIR = tempfile.TemporaryDirectory()
    OJPP_DIR = tempfile.TemporaryDirectory()
    CACHE_PATH = Path(CACHE_DIR.name)
    OJPP_PATH = Path(OJPP_DIR.name)
    ZIP_PATH = Path(ZIP_DIR.name)
    APMS_PATH = Path(APMS_DIR.name)
else:
    CACHE_PATH = Path('cache')
    ZIP_PATH = Path('ijpp_gtfs')
    APMS_PATH = Path('apms_gtfs')

INPUT = 'ijpp_netex_gtfs.zip'

#################
#   LOAD GTFS   #
#################

# get ijpp_netex_gtfs.zip artifact from the previous stage
print('Downloading GTFS...', end="")
if not Path(INPUT).exists():
    raise FileNotFoundError(f"GTFS file '{INPUT}' not found!")
shutil.copy(INPUT, CACHE_PATH / INPUT)
print('DONE')

INPUT = CACHE_PATH / INPUT


###################
#   LOAD ROUTES   #
###################

print('Extracting GTFS...')
shutil.unpack_archive(INPUT, ZIP_PATH)

print('Reading routes')
routes = pd.read_csv(ZIP_PATH / 'routes.txt')

print('Fixing names')

route_color_map = {
    # Arriva
    'SI:SI0:Operator:00000000-1177-0001-0000-000000000463:IJPP': ('24B7C7', 'FFFFFF'),
    # Nomago
    'SI:SI0:Operator:00000000-1177-0001-0000-00000000045f:IJPP': ('FBB900', '004899'),
    # APMS
    'SI:SI0:Operator:00000000-1177-0001-0000-000000000461:IJPP': ('0077BE', 'FFFFFF'),
    # SŽ
    'SI:SI0:Operator:00000000-1177-0001-0000-000000000489:IJPP': ('29ACE2', 'FFFFFF'),
    # LPP
    'SI:SI0:Operator:00000000-1177-0001-0000-00000000045e:IJPP': ('207C4C', 'FFFFFF'),
}

def fix_route_short_name(name):
    """Fixes route short names so they are actually short and useful"""
    name = str(name)
    if len(name) > 4:
        # Trim off first 5 characters
        return name[5:]
    else:
        # Make 4 characters long
        return name.zfill(4)

def parse_ijpp_guid(guid_str):
    try:
        # Remove hyphens from the UUID string
        clean_guid_str = guid_str.replace('-', '')

        # Convert the cleaned string (hexadecimal) into bytes
        guid_bytes = bytes.fromhex(clean_guid_str)

        # Extract the last 4 bytes and interpret them as a big-endian 32-bit integer
        ijpp_id = struct.unpack('>I', guid_bytes[-4:])[0]

        return ijpp_id
    except Exception as e:
        raise ValueError(f"Error parsing IJPP GUID: {e}")

def realis_to_ijpp(realis_id):
    try:
        # Split the realis_id into parts
        parts = realis_id.split(':')

        # Ensure the realis_id has exactly 5 parts
        if len(parts) != 5:
            raise ValueError(f"Invalid realis_id: {realis_id}")

        # Extract the GUID from the parts
        guid = parts[3]

        # Parse the IJPP ID from the GUID
        ijpp_id = parse_ijpp_guid(guid)

        return str(ijpp_id)
    except Exception as e:
        return ""

def split_out_hash(id):
    return id.split(':')[3]


print('    Fixing route short names')
routes['route_short_name'] = routes['route_short_name'].apply(fix_route_short_name)
routes['route_long_name'] = ""

print('    Assigning colors to routes by agency')
for agency_id, (color, text_color) in route_color_map.items():
    routes.loc[routes['agency_id'].astype(str) == agency_id, 'route_color'] = color
    routes.loc[routes['agency_id'].astype(str) == agency_id, 'route_text_color'] = text_color

print('    Replacing NeTEx IDs with IJPP IDs')
routes['route_id'] = routes['route_id'].apply(realis_to_ijpp)
routes['agency_id'] = routes['agency_id'].apply(realis_to_ijpp)

print('Writing routes')
routes.to_csv(ZIP_PATH / 'routes.txt', index=False)

print('Reading trips')
trips = pd.read_csv(ZIP_PATH / 'trips.txt')

print('    Replacing NeTEx IDs with IJPP IDs')
trips['route_id'] = trips['route_id'].apply(realis_to_ijpp)
trips['trip_id'] = trips['trip_id'].apply(realis_to_ijpp)
trips['shape_id'] = trips['shape_id'].apply(split_out_hash)

print('Reading stop times')
stop_times = pd.read_csv(ZIP_PATH / 'stop_times.txt')

print('    Replacing NeTEx IDs with IJPP IDs')
stop_times['trip_id'] = stop_times['trip_id'].apply(realis_to_ijpp)
stop_times['stop_id'] = stop_times['stop_id'].apply(realis_to_ijpp)

print('Writing stop times')
stop_times.to_csv(ZIP_PATH / 'stop_times.txt', index=False)

print('Reading stops')
stops = pd.read_csv(ZIP_PATH / 'stops.txt')

print('    Replacing NeTEx IDs with IJPP IDs')
stops['stop_id'] = stops['stop_id'].apply(realis_to_ijpp)

# Remove 01, 02, ... etc from end of stop names
stops['stop_name'] = stops['stop_name'].apply(lambda x: x[:-3])

print('Writing stops')
stops.to_csv(ZIP_PATH / 'stops.txt', index=False)

# Calculate the trip headsigns as First Stop - Last Stop
print('   Calculating trip headsigns')

# Assuming `stops` DataFrame contains 'stop_id' and 'stop_name'
stop_id_to_name = stops.set_index('stop_id')['stop_name'].to_dict()

# Sort stop_times by 'trip_id' and 'stop_sequence' to ensure proper ordering
stop_times_sorted = stop_times.sort_values(by=['trip_id', 'stop_sequence'])

# Get the first and last stop sequences and stop IDs for each trip
first_stops = stop_times_sorted.groupby('trip_id').first().reset_index()
last_stops = stop_times_sorted.groupby('trip_id').last().reset_index()

# Calculate the median stop sequence for each trip
median_stop_sequence = stop_times_sorted.groupby('trip_id')['stop_sequence'].median().reset_index()
median_stop_sequence.columns = ['trip_id', 'median_stop_sequence']

# Merge with stop_times to get the middle stops
middle_stops = pd.merge(stop_times_sorted, median_stop_sequence, on='trip_id')

# Round the median to the nearest stop sequence if it's not an integer
middle_stops['median_stop_sequence'] = middle_stops['median_stop_sequence'].round().astype(int)

# Filter for rows that match the median stop sequence
middle_stops = middle_stops[middle_stops['stop_sequence'] == middle_stops['median_stop_sequence']]

# Map stop IDs to stop names
first_stops['first_stop_name'] = first_stops['stop_id'].map(stop_id_to_name)
last_stops['last_stop_name'] = last_stops['stop_id'].map(stop_id_to_name)
middle_stops['middle_stop_name'] = middle_stops['stop_id'].map(stop_id_to_name)

# Ensure the names are correctly mapped
print(first_stops[['trip_id', 'stop_id', 'first_stop_name']].head())
print(last_stops[['trip_id', 'stop_id', 'last_stop_name']].head())
print(middle_stops[['trip_id', 'stop_id', 'middle_stop_name']].head())

# Merge the first and last stops
trip_headsigns = pd.merge(first_stops[['trip_id', 'first_stop_name']],
                          last_stops[['trip_id', 'last_stop_name']],
                          on='trip_id')

trip_headsigns = pd.merge(trip_headsigns, middle_stops[['trip_id', 'middle_stop_name']], on='trip_id', how='left')

# Create the trip_headsign as 'stopname1 - stopname2' if stopname1 != stopname2, else 'stopname1 - stopname_middle - stopname2'
trip_headsigns['trip_headsign'] = trip_headsigns.apply(lambda x: f"{x['first_stop_name']} - {x['last_stop_name']}" if x['first_stop_name'] != x['last_stop_name'] else f"{x['first_stop_name']} - {x['middle_stop_name']} - {x['last_stop_name']}", axis=1)

# Merge the headsigns back into the trips DataFrame
trips = trips.merge(trip_headsigns[['trip_id', 'trip_headsign']], on='trip_id', how='left')

# Rename the existing 'trip_short_name' column before adding the new one
if 'trip_short_name' in trips.columns:
    trips.rename(columns={'trip_short_name': 'trip_short_name_old'}, inplace=True)

# Add a placeholder 'trip_short_name' as the 'last_stop_name'
trips = trips.merge(last_stops[['trip_id', 'last_stop_name']], on='trip_id', how='left')

# Ensure 'bikes_allowed' is set to integers
print('Setting bikes_allowed')
if 'bikes_allowed' in trips.columns:
    trips['bikes_allowed'] = trips['bikes_allowed'].fillna(0).astype(int)
else:
    trips['bikes_allowed'] = 0

# Ensure the correct trip_headsign is kept
if 'trip_headsign_x' in trips.columns and 'trip_headsign_y' in trips.columns:
    trips.drop(columns=['trip_headsign_x'], inplace=True)
    trips.rename(columns={'trip_headsign_y': 'trip_headsign'}, inplace=True)

# Drop the old 'trip_short_name' column if it exists
if 'trip_short_name_old' in trips.columns:
    trips.drop(columns=['trip_short_name_old'], inplace=True)

if 'trip_short_name' in trips.columns:
    trips.drop(columns=['trip_short_name'], inplace=True)

if 'last_stop_name' in trips.columns:
    trips.drop(columns=['last_stop_name'], inplace=True)

if 'middle_stop_name' in trips.columns:
    trips.drop(columns=['middle_stop_name'], inplace=True)

print('Writing trips')
trips.to_csv(ZIP_PATH / 'trips.txt', index=False)

print('Fixing agencies')
agencies = pd.read_csv(ZIP_PATH / 'agency.txt')
print('    Replacing NeTEx IDs with IJPP IDs')
agencies['agency_id'] = agencies['agency_id'].apply(realis_to_ijpp)

print('Writing agencies')
agencies.to_csv(ZIP_PATH / 'agency.txt', index=False)

print('Fixing shapes')
shapes = pd.read_csv(ZIP_PATH / 'shapes.txt')
print('    Replacing NeTEx IDs with IJPP IDs')
shapes['shape_id'] = shapes['shape_id'].apply(split_out_hash)
print('Writing shapes')
shapes.to_csv(ZIP_PATH / 'shapes.txt', index=False)

# Backup original routes DataFrame
original_routes = routes.copy()

# Compress the GTFS data for IJPP only
print('Compressing...', end="")
shutil.make_archive(str(OUTPUT).removesuffix('.zip'), 'zip', ZIP_PATH)
print('DONE')

# Create a new agency file with only one agency (IJPP)
agency_data = {
    'agency_id': ['1'],
    'agency_name': ['IJPP'],
    'agency_url': ['http://ijpp.si'],
    'agency_timezone': ['Europe/Ljubljana']
}
agency_df = pd.DataFrame(agency_data)

print('Writing new agency file')
agency_file_path = ZIP_PATH / 'agency.txt'
agency_df.to_csv(agency_file_path, index=False)

# Reallocate all routes to the new agency ID for IJPP
print('Reallocating routes to the new agency for IJPP')
routes['agency_id'] = '1'
routes.to_csv(ZIP_PATH / 'routes.txt', index=False)

# Create a new zip file for IJPP only
print('Creating new GTFS zip with single agency...')
shutil.make_archive(str(NEW_OUTPUT).removesuffix('.zip'), 'zip', ZIP_PATH)
print('New GTFS zip created as ijpp_gtfs_tc.zip')

# Restore the original routes DataFrame for APMS
routes = original_routes.copy()

# Create a new zip file for APMS only
print('Filtering and creating GTFS zip for APMS...')
apms_routes = routes[routes['agency_id'].astype(str) == realis_to_ijpp('SI:SI0:Operator:00000000-1177-0001-0000-000000000461:IJPP')]
apms_trips = trips[trips['route_id'].isin(apms_routes['route_id'])]
apms_stop_times = stop_times[stop_times['trip_id'].isin(apms_trips['trip_id'])]
apms_stops = stops[stops['stop_id'].isin(apms_stop_times['stop_id'])]
apms_shapes = shapes[shapes['shape_id'].isin(apms_trips['shape_id'])]
# Copy all relevant files to the APMS directory
shutil.copytree(ZIP_PATH, APMS_PATH, dirs_exist_ok=True)

# Overwrite the filtered files in the APMS directory
apms_routes.to_csv(APMS_PATH / 'routes.txt', index=False)
apms_trips.to_csv(APMS_PATH / 'trips.txt', index=False)
apms_stop_times.to_csv(APMS_PATH / 'stop_times.txt', index=False)
apms_stops.to_csv(APMS_PATH / 'stops.txt', index=False)
apms_shapes.to_csv(APMS_PATH / 'shapes.txt', index=False)

# Ensure the agency file includes only APMS
print('Filtering agency file for APMS')
agency_apms = {
    'agency_id': [realis_to_ijpp('SI:SI0:Operator:00000000-1177-0001-0000-000000000461:IJPP')],
    'agency_name': ['Avtobusni promet Murska Sobota d.d.'],
    'agency_url': ['http://apms.si'],
    'agency_timezone': ['Europe/Ljubljana']
}
apms_agency = pd.DataFrame(agency_apms)
apms_agency.to_csv(APMS_PATH / 'agency.txt', index=False)

# Create the APMS GTFS zip
shutil.make_archive(str(APMS_OUTPUT).removesuffix('.zip'), 'zip', APMS_PATH)
print('APMS GTFS zip created as apms_gtfs.zip')

if CLEANUP:
    CACHE_DIR.cleanup()
    ZIP_DIR.cleanup()
    APMS_DIR.cleanup()
