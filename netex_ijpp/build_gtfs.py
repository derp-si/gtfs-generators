from itertools import chain
from pathlib import Path
import shutil
import tempfile
import math
import tqdm
import requests
import pandas as pd
from lxml import etree
from datetime import datetime, timedelta
import hashlib
import io

##############
#  CLASSES   #
##############


class Stop:
    def __init__(self, stop_id, stop_code, stop_name, stop_lat, stop_lon):
        self.stop_id = stop_id
        self.stop_code = stop_code
        self.stop_name = stop_name
        self.stop_lat = stop_lat
        self.stop_lon = stop_lon


class Calendar:
    def __init__(self, service_id, start_date, end_date, days):
        self.service_id = service_id
        self.start_date = start_date
        self.end_date = end_date
        self.monday = 1 if days[0] else 0
        self.tuesday = 1 if days[1] else 0
        self.wednesday = 1 if days[2] else 0
        self.thursday = 1 if days[3] else 0
        self.friday = 1 if days[4] else 0
        self.saturday = 1 if days[5] else 0
        self.sunday = 1 if days[6] else 0
        self.holiday = 1 if days[7] else 0


class CalendarDate:
    def __init__(self, service_id, date, exception_type):
        self.service_id = service_id
        self.date = date
        self.exception_type = exception_type


class Agency:
    def __init__(self, agency_id, agency_name, agency_url, agency_timezone):
        self.agency_id = agency_id
        self.agency_name = agency_name
        self.agency_url = agency_url or "https://ijpp.si"
        self.agency_timezone = agency_timezone


class Route:
    def __init__(
        self, route_id, agency_id, route_short_name, route_long_name, route_type
    ):
        self.route_id = route_id
        self.agency_id = agency_id
        self.route_short_name = route_short_name
        self.route_long_name = route_long_name
        self.route_type = route_type


class Trip:
    def __init__(self, route_id, service_id, trip_id, shape_id, trip_headsign):
        self.route_id = route_id
        self.service_id = service_id
        self.trip_id = trip_id
        self.shape_id = shape_id
        self.trip_headsign = trip_headsign

    def __str__(self):
        return f"{self.route_id} - {self.service_id} - {self.trip_id} - {self.shape_id} - {self.trip_headsign}"


class Shape:
    def __init__(self, shape_id, shape_pt_lat, shape_pt_lon, shape_pt_sequence):
        self.shape_id = shape_id
        self.shape_pt_lat = shape_pt_lat
        self.shape_pt_lon = shape_pt_lon
        self.shape_pt_sequence = shape_pt_sequence


class StopTime:
    def __init__(self, trip_id, arrival_time, departure_time, stop_id, stop_sequence):
        self.trip_id = trip_id
        self.arrival_time = arrival_time
        self.departure_time = departure_time
        self.stop_id = stop_id
        self.stop_sequence = stop_sequence
        self.timepoint = 1


yesterday = datetime.now() - timedelta(days=1)
in_a_year = datetime.now() + timedelta(days=365)

class DayType:
    def __init__(self, id, name, code, days_of_week):
        self.id = id
        self.name = name
        self.code = code
        self.monday = 1 if "Monday" in days_of_week else 0
        self.tuesday = 1 if "Tuesday" in days_of_week else 0
        self.wednesday = 1 if "Wednesday" in days_of_week else 0
        self.thursday = 1 if "Thursday" in days_of_week else 0
        self.friday = 1 if "Friday" in days_of_week else 0
        self.saturday = 1 if "Saturday" in days_of_week else 0
        self.sunday = 1 if "Sunday" in days_of_week else 0
        self.holiday = 1 if "Holiday" in days_of_week else 0
        self.operating_periods = []
        self.operating_dates = []

    def get_period_range(self):
        start_date = min([period.start_date for period in self.operating_periods]) if self.operating_periods else yesterday
        end_date = max([period.end_date for period in self.operating_periods]) if self.operating_periods else in_a_year
        return start_date, end_date

    def _iter_dates(self, start_date, end_date):
        current_date = start_date
        while current_date <= end_date:
            yield current_date
            current_date += timedelta(days=1)

    def get_dates(self):
        start_date, end_date = self.get_period_range()
        dates = []
        for date in self._iter_dates(start_date, end_date):
            found = False
            # Check if we have an operating date for this date
            for operating_date in self.operating_dates:
                if operating_date.date == date:
                    # If it is available, we add it
                    if operating_date.is_available:
                        dates.append(date)
                    found = True
                    break
            if found:
                continue
            # Check if the date falls into a operating period
            for operating_period in self.operating_periods:
                if operating_period.check_date(date):
                    # If this is a holiday date, check if the daytype allows it
                    if date.strftime("%d.%MM.%Y") in holidays and self.holiday == 1:
                        dates.append(date)
                    elif date.weekday() == 0 and self.monday == 1:
                        dates.append(date)
                    elif date.weekday() == 1 and self.tuesday == 1:
                        dates.append(date)
                    elif date.weekday() == 2 and self.wednesday == 1:
                        dates.append(date)
                    elif date.weekday() == 3 and self.thursday == 1:
                        dates.append(date)
                    elif date.weekday() == 4 and self.friday == 1:
                        dates.append(date)
                    elif date.weekday() == 5 and self.saturday == 1:
                        dates.append(date)
                    elif date.weekday() == 6 and self.sunday == 1:
                        dates.append(date)

        return dates


class OperatingPeriod:
    def __init__(self, id, start_date, end_date):
        self.id = id
        self.start_date = start_date
        self.end_date = end_date

    def check_date(self, date):
        return self.start_date <= date <= self.end_date

class OperatingDate:
    def __init__(self, id, date, is_available):
        self.id = id
        self.date = date
        self.is_available = is_available



##############
#   CONFIG   #
##############
CLEANUP = True
OUTPUT = Path("ijpp_netex_gtfs.zip")

if CLEANUP:
    # Take current working directory as cache directory
    CACHE_DIR = tempfile.TemporaryDirectory()
    CACHE_PATH = Path(CACHE_DIR.name)
else:
    CACHE_PATH = Path("cache")

holidays = []
holiday_raw = requests.get('https://podatki.gov.si/dataset/ada88e06-14a2-49c4-8748-3311822e3585/resource/eb8b25ea-5c00-4817-a670-26e1023677c6/download/seznampraznikovindelaprostihdni20002030.csv')
holiday_raw.raise_for_status()
holiday_raw.encoding = 'utf-8'
holidays_pd = pd.read_csv(io.StringIO(holiday_raw.text), sep=';', encoding='utf-8')
# Convert holidays into an list of dates
holidays = [row['DATUM'] for index, row in holidays_pd.iterrows() if row['DELA_PROST_DAN'] == 'da']


#################
# 1. Load NeTEx #
#################
print("Downloading NeTEx...")

print("    Downloading network...", end="")
NETWORK_ZIP = CACHE_PATH / "NETEX_PI_01_SI_NAP_NETWORK.zip"
resp = requests.get("http://gtfs.derp.si/ijpp_netex_network.zip")
resp.raise_for_status()
assert len(resp.content) > 10000, "GTFS file is suspiciously small!"
with open(NETWORK_ZIP, "wb") as f:
    f.write(resp.content)
print("DONE")

print("    Unzipping network...", end="")
shutil.unpack_archive(NETWORK_ZIP, extract_dir=CACHE_PATH / "network")
print("    DONE")

print("Processing network...")
print("    Finding network file...", end="")
# Find the file with network
network_file = next((CACHE_PATH / "network").rglob("*.XML"))
print("    DONE")
print("    Parsing network file...", end="")
# Parse the network file
network = etree.parse(network_file)
print("    DONE")
print("    Extracting stops...", end="")
# Extract the Quay elements from the stops file
quays = network.xpath("//ns:Quay", namespaces={"ns": "http://www.netex.org.uk/netex"})
print(" extracted", len(quays), "stops...", end="")
stops = []
for quay in quays:
    stop_id = quay.attrib["id"]
    stop_code = quay.xpath(
        "ns:PrivateCode", namespaces={"ns": "http://www.netex.org.uk/netex"}
    )[0].text
    stop_name = quay.xpath(
        "ns:Name", namespaces={"ns": "http://www.netex.org.uk/netex"}
    )[0].text
    stop_lat = quay.xpath(
        "ns:Centroid/ns:Location/ns:Latitude",
        namespaces={"ns": "http://www.netex.org.uk/netex"},
    )[0].text
    stop_lon = quay.xpath(
        "ns:Centroid/ns:Location/ns:Longitude",
        namespaces={"ns": "http://www.netex.org.uk/netex"},
    )[0].text
    stops.append(
        Stop(
            stop_id=stop_id,
            stop_code=stop_code,
            stop_name=stop_name,
            stop_lat=stop_lat,
            stop_lon=stop_lon,
        )
    )
print("    DONE")
operators_netex = network.xpath(
    "//ns:Operator", namespaces={"ns": "http://www.netex.org.uk/netex"}
)
print(" extracted", len(operators_netex), "operators...", end="")
operators = []
for operator in operators_netex:
    operator_id = operator.attrib["id"]
    operator_name = operator.xpath(
        "ns:Name", namespaces={"ns": "http://www.netex.org.uk/netex"}
    )[0].text
    operator_url = operator.xpath(
        "ns:ContactDetails/ns:Url", namespaces={"ns": "http://www.netex.org.uk/netex"}
    )[0].text
    operator_timezone = (
        "Europe/Ljubljana"  # Hardcoded, since NeTEx provides it as UTC+1
    )
    operators.append(
        Agency(
            agency_id=operator_id,
            agency_name=operator_name,
            agency_url=operator_url,
            agency_timezone=operator_timezone,
        )
    )
print("    DONE")
print("    Extracting network...", end="")
# Extract the Line elements from the network file
service_calendar_frames = network.xpath(
    "//ns:ServiceCalendarFrame", namespaces={"ns": "http://www.netex.org.uk/netex"}
)
print(" extracted", len(service_calendar_frames), "service calendar frames...", end="")

day_types_list = []
operating_periods_list = []
operating_dates_list = []
# Helper dictionaries
day_types_dict = {}     # ref: obj
operating_periods_dict = {}  # ref: obj
operating_dates_dict = {}    # ref: obj

referenced_periods = {}
referenced_dates = {}

# Iterate over service calendar frames to process DayTypes and related entities
for service_calendar_frame in service_calendar_frames:
    day_types = service_calendar_frame.xpath(
        "//ns:DayType", namespaces={"ns": "http://www.netex.org.uk/netex"}
    )
    for day_type in day_types:
        day_type_id = day_type.attrib["id"]
        day_type_name = day_type.xpath(
            "ns:Name", namespaces={"ns": "http://www.netex.org.uk/netex"}
        )[0].text
        day_type_code = day_type.xpath(
            "ns:PrivateCode", namespaces={"ns": "http://www.netex.org.uk/netex"}
        )[0].text if day_type.xpath(
            "ns:PrivateCode", namespaces={"ns": "http://www.netex.org.uk/netex"}
        ) else None
        days_of_week = day_type.xpath(
            "ns:properties/ns:PropertyOfDay/ns:DaysOfWeek",
            namespaces={"ns": "http://www.netex.org.uk/netex"},
        )
        holiday_types = day_type.xpath(
            "ns:properties/ns:PropertyOfDay/ns:HolidayTypes",
            namespaces={"ns": "http://www.netex.org.uk/netex"},
        )
        days_of_week = days_of_week[0].text.split() if days_of_week else []
        if holiday_types and "Holiday" in holiday_types[0].text:
            days_of_week.append("Holiday")

        day_type_obj = DayType(
            id=day_type_id,
            name=day_type_name,
            code=day_type_code,
            days_of_week=days_of_week,
        )
        day_types_list.append(day_type_obj)
        day_types_dict[day_type_id] = day_type_obj

    operating_periods = service_calendar_frame.xpath(
        "//ns:OperatingPeriod", namespaces={"ns": "http://www.netex.org.uk/netex"}
    )
    for operating_period in operating_periods:
        period_id = operating_period.attrib["id"]
        start_date = datetime.strptime(
            operating_period.xpath(
                "ns:FromDate", namespaces={"ns": "http://www.netex.org.uk/netex"}
            )[0].text.split("T")[0],
            "%Y-%m-%d",
        )
        end_date = datetime.strptime(
            operating_period.xpath(
                "ns:ToDate", namespaces={"ns": "http://www.netex.org.uk/netex"}
            )[0].text.split("T")[0],
            "%Y-%m-%d",
        )
        operating_periods_list.append(
            OperatingPeriod(id=period_id, start_date=start_date, end_date=end_date)
        )
        operating_periods_dict[period_id] = operating_periods_list[-1]

    operating_days = service_calendar_frame.xpath(
        "//ns:OperatingDay", namespaces={"ns": "http://www.netex.org.uk/netex"}
    )
    for operating_day in operating_days:
        operating_day_id = operating_day.attrib["id"]
        date = datetime.strptime(
            operating_day.xpath(
                "ns:CalendarDate", namespaces={"ns": "http://www.netex.org.uk/netex"}
            )[0].text,
            "%Y-%m-%d",
        )
        is_available = (
            True
            if operating_day.xpath(
                "ns:isAvailable", namespaces={"ns": "http://www.netex.org.uk/netex"}
            )
            else False
        )
        operating_dates_list.append(
            OperatingDate(id=operating_day_id, date=date, is_available=is_available)
        )
        operating_dates_dict[operating_day_id] = operating_dates_list[-1]

    daytype_assignments = service_calendar_frame.xpath(
        "//ns:DayTypeAssignment", namespaces={"ns": "http://www.netex.org.uk/netex"}
    )
    for daytype_assignment in daytype_assignments:
        day_type_ref = daytype_assignment.xpath(
            "ns:DayTypeRef", namespaces={"ns": "http://www.netex.org.uk/netex"}
        )[0].attrib["ref"]
        operating_period_ref = daytype_assignment.xpath(
            "ns:OperatingPeriodRef", namespaces={"ns": "http://www.netex.org.uk/netex"}
        )
        operating_day_ref = daytype_assignment.xpath(
            "ns:OperatingDayRef", namespaces={"ns": "http://www.netex.org.uk/netex"}
        )

        if operating_period_ref:
            day_types_dict[day_type_ref].operating_periods.append(
                operating_periods_dict[operating_period_ref[0].attrib["ref"]]
            )
            referenced_periods[operating_period_ref[0].attrib["ref"]] = True
        elif operating_day_ref:
            day_types_dict[day_type_ref].operating_dates.append(
                operating_dates_dict[operating_day_ref[0].attrib["ref"]]
            )
            referenced_dates[operating_day_ref[0].attrib["ref"]] = True
        else:
            raise ValueError("DayTypeAssignment has neither OperatingPeriodRef nor OperatingDayRef!")

# Iterate over all periods and dates and print which ones are not referenced
for period in operating_periods_list:
    if period.id not in referenced_periods:
        print(f"Period {period.id} ({period.start_date} - {period.end_date}) is not referenced in any DayTypeAssignment!")
for date in operating_dates_list:
    if date.id not in referenced_dates:
        print(f"Date {date.id} ({date.date}) is not referenced in any DayTypeAssignment!")

# Get the routes (lines)
lines = network.xpath("//ns:Line", namespaces={"ns": "http://www.netex.org.uk/netex"})
print("    Extracting lines... number of lines:", len(lines), end="")
routes = []
for line in lines:
    line_id = line.attrib["id"]
    operator_ref = line.xpath(
        "ns:OperatorRef", namespaces={"ns": "http://www.netex.org.uk/netex"}
    )[0].attrib["ref"]
    public_code = line.xpath(
        "ns:PublicCode", namespaces={"ns": "http://www.netex.org.uk/netex"}
    )[0].text
    line_name = line.xpath(
        "ns:Name", namespaces={"ns": "http://www.netex.org.uk/netex"}
    )[0].text
    line_type = line.xpath(
        "ns:TransportMode", namespaces={"ns": "http://www.netex.org.uk/netex"}
    )[0].text
    # It can either be bus or train, so replace it with the GTFS values
    if line_type == "bus":
        line_type = 3
    elif line_type == "rail":
        line_type = 2
    routes.append(
        Route(
            route_id=line_id,
            agency_id=operator_ref,
            route_short_name=public_code,
            route_long_name=line_name,
            route_type=line_type,
        )
    )
print("    DONE")
# Get the trips (journeys)
routes_netex = network.xpath(
    "//ns:Route", namespaces={"ns": "http://www.netex.org.uk/netex"}
)
print("    Extracting routes... number of routes:", len(routes_netex))
routes_netex_dict = {}
for route in routes_netex:
    route_id = route.attrib["id"]
    line_ref = route.xpath(
        "ns:LineRef", namespaces={"ns": "http://www.netex.org.uk/netex"}
    )[0].attrib["ref"]
    routes_netex_dict[route_id] = {"route": route, "line_ref": line_ref}

route_points = network.xpath(
    "//ns:RoutePoint", namespaces={"ns": "http://www.netex.org.uk/netex"}
)
print("    Extracting route points... number of route points:", len(route_points))
route_points_dict = {}
for route_point in route_points:
    route_point_id = route_point.attrib["id"]
    route_points_dict[route_point_id] = {
        "id": route_point_id,
        "name": route_point.xpath(
            "ns:Name", namespaces={"ns": "http://www.netex.org.uk/netex"}
        )[0].text,
        "point_projection": route_point.xpath(
            "ns:projections/ns:PointProjection",
            namespaces={"ns": "http://www.netex.org.uk/netex"},
        )[0].attrib["id"],
        "scheduled_stop_point_ref": route_point.xpath(
            "ns:projections/ns:PointProjection/ns:ProjectToPointRef",
            namespaces={"ns": "http://www.netex.org.uk/netex"},
        )[0].attrib["ref"],
    }

scheduled_stop_points = network.xpath(
    "//ns:ScheduledStopPoint", namespaces={"ns": "http://www.netex.org.uk/netex"}
)
print(
    "    Extracting scheduled stop points... number of scheduled stop points:",
    len(scheduled_stop_points),
)
scheduled_stop_points_dict = {}
for scheduled_stop_point in scheduled_stop_points:
    scheduled_stop_point_id = scheduled_stop_point.attrib["id"]
    scheduled_stop_points_dict[scheduled_stop_point_id] = {
        "id": scheduled_stop_point_id,
        "name": scheduled_stop_point.xpath(
            "ns:Name", namespaces={"ns": "http://www.netex.org.uk/netex"}
        )[0].text,
    }

service_links = network.xpath(
    "//ns:ServiceLink", namespaces={"ns": "http://www.netex.org.uk/netex"}
)
print("    Extracting service links... number of service links:", len(service_links))
service_links_dict = {}
# Service link dict is structured as:
for service_link in tqdm.tqdm(
    service_links, desc="Service links - processing", total=len(service_links)
):
    service_link_id = service_link.attrib["id"]
    from_point_ref = service_link.xpath(
        "ns:FromPointRef", namespaces={"ns": "http://www.netex.org.uk/netex"}
    )[0].attrib["ref"]
    to_point_ref = service_link.xpath(
        "ns:ToPointRef", namespaces={"ns": "http://www.netex.org.uk/netex"}
    )[0].attrib["ref"]
    distance = service_link.xpath(
        "ns:Distance", namespaces={"ns": "http://www.netex.org.uk/netex"}
    )
    if len(distance) > 0:
        distance = distance[0].text
    else:
        distance = 0
    linestring = service_link.xpath(
        "p10:LineString/p10:pos",
        namespaces={
            "p10": "http://www.opengis.net/gml/3.2",
            "ns": "http://www.netex.org.uk/netex",
        },
    )

    service_links_dict[service_link_id] = {
        "from_point_ref": from_point_ref,
        "to_point_ref": to_point_ref,
        "linestring": [pos.text.split(" ") for pos in linestring],
        "distance": distance,
        "id": service_link_id,
    }

journey_patterns = network.xpath(
    "//ns:ServiceJourneyPattern", namespaces={"ns": "http://www.netex.org.uk/netex"}
)
print(
    "    Extracting journey patterns... number of journey patterns:",
    len(journey_patterns),
)
journey_patterns_dict = {}
for journey_pattern in tqdm.tqdm(
    journey_patterns, desc="Journey patterns - processing", total=len(journey_patterns)
):
    journey_pattern_id = journey_pattern.attrib["id"]
    route_ref = journey_pattern.xpath(
        "ns:RouteRef", namespaces={"ns": "http://www.netex.org.uk/netex"}
    )[0].attrib["ref"]
    stop_points_in_journey_pattern = journey_pattern.xpath(
        "ns:pointsInSequence/ns:StopPointInJourneyPattern",
        namespaces={"ns": "http://www.netex.org.uk/netex"},
    )
    # Map them to tuples of StopPointInJourneyPattern.id and ScheduledStopPointRef.ref
    stop_points_in_journey_pattern = {
        stop_point.attrib["id"]: stop_point.xpath(
            "ns:ScheduledStopPointRef",
            namespaces={"ns": "http://www.netex.org.uk/netex"},
        )[0].attrib["ref"]
        for stop_point in stop_points_in_journey_pattern
    }
    service_links_in_journey_pattern = journey_pattern.xpath(
        "ns:linksInSequence/ns:ServiceLinkInJourneyPattern/ns:ServiceLinkRef",
        namespaces={"ns": "http://www.netex.org.uk/netex"},
    )
    # Map them to list of ServiceLinkRef.ref
    service_links_in_journey_pattern = [
        service_link.attrib.get("ref")
        for service_link in service_links_in_journey_pattern
    ]
    journey_patterns_dict[journey_pattern_id] = {
        "id": journey_pattern_id,
        "name": journey_pattern.xpath(
            "ns:Name", namespaces={"ns": "http://www.netex.org.uk/netex"}
        )[0].text,
        "route_ref": route_ref,
        "stop_points": stop_points_in_journey_pattern,
        "service_links": service_links_in_journey_pattern,
    }

journeys = network.xpath(
    "//ns:ServiceJourney", namespaces={"ns": "http://www.netex.org.uk/netex"}
)
print("    Extracting journeys... number of journeys:", len(journeys))
stop_name_to_id = {stop.stop_name: stop.stop_id for stop in stops}
trips = []
stop_times = []
# Shapemap is of the format:
# hash - sha1(list of service link ids)
# shape - list of points in GTFS format
shape_map = {}

journeys_with_more_than_one_day_type = []
for journey in tqdm.tqdm(journeys, desc="Journeys - processing", total=len(journeys)):
    journey_id = journey.attrib["id"]
    pattern_ref = journey.xpath(
        "ns:ServiceJourneyPatternRef",
        namespaces={"ns": "http://www.netex.org.uk/netex"},
    )[0].attrib["ref"]
    pattern = journey_patterns_dict[pattern_ref]
    route_ref = journey_patterns_dict[pattern_ref]["route_ref"]
    line_ref = routes_netex_dict[route_ref]["line_ref"]
    day_types = journey.xpath(
        "ns:dayTypes/ns:DayTypeRef", namespaces={"ns": "http://www.netex.org.uk/netex"}
    )
    # Map them to list of DayTypeRef.ref
    day_types = [day_type.attrib["ref"] for day_type in day_types]
    # Remove duplicates
    day_types = list(set(day_types))
    # If we have anything but one day type, we skip the journey
    if len(day_types) == 0:
        print(
            "Skipping journey",
            journey_id,
            "because it has",
            len(day_types),
            "day types",
        )
        continue
    elif len(day_types) > 1:
        journeys_with_more_than_one_day_type.append(journey_id)

    for day_type in day_types:
        passing_times = journey.xpath(
            "ns:passingTimes/ns:TimetabledPassingTime",
            namespaces={"ns": "http://www.netex.org.uk/netex"},
        )
        trip_passes_midnight = False
        previous_departure_hour = 0
        passed_service_links = journey_patterns_dict[pattern_ref]["service_links"]
        for i, passing_time in enumerate(passing_times):
            arrival_time = passing_time.xpath(
                "ns:ArrivalTime", namespaces={"ns": "http://www.netex.org.uk/netex"}
            )
            departure_time = passing_time.xpath(
                "ns:DepartureTime", namespaces={"ns": "http://www.netex.org.uk/netex"}
            )
            arrival_time = (
                arrival_time[0].text
                if len(arrival_time) > 0
                else departure_time[0].text
            )
            departure_time = (
                departure_time[0].text if len(departure_time) > 0 else arrival_time
            )
            # Check if the arrival and departure time are mixed up by some chance, and if they are, rotate them
            if arrival_time > departure_time:
                arrival_time, departure_time = departure_time, arrival_time
            # Check if the trip passes midnight and adjust the times accordingly
            arrival_hour = int(arrival_time.split(":")[0])
            departure_hour = int(departure_time.split(":")[0])
            if not trip_passes_midnight and (
                departure_hour < arrival_hour
                or (arrival_hour < previous_departure_hour)
            ):
                trip_passes_midnight = True
            if trip_passes_midnight:
                arrival_hour, arrival_minute, arrival_second = map(
                    int, arrival_time.split(":")
                )
                departure_hour, departure_minute, departure_second = map(
                    int, departure_time.split(":")
                )
                arrival_time = (
                    f"{arrival_hour + 24:02d}:{arrival_minute:02d}:{arrival_second:02d}"
                    if arrival_hour < 23
                    else f"{arrival_hour:02d}:{arrival_minute:02d}:{arrival_second:02d}"
                )
                departure_time = (
                    f"{departure_hour + 24:02d}:{departure_minute:02d}:{departure_second:02d}"
                    if departure_hour < 23
                    else f"{departure_hour:02d}:{departure_minute:02d}:{departure_second:02d}"
                )
            # If arrival > departure, reverse them
            if arrival_time > departure_time:
                arrival_time, departure_time = departure_time, arrival_time
            # If there is a delta more than 1 hour, copy over the departure time to the arrival time
            arrival_time_secs = sum(
                [
                    int(x) * 60**i
                    for i, x in enumerate(reversed(arrival_time.split(":")))
                ]
            )
            departure_time_secs = sum(
                [
                    int(x) * 60**i
                    for i, x in enumerate(reversed(departure_time.split(":")))
                ]
            )
            if abs(departure_time_secs - arrival_time_secs) > 3600:
                # Copy it over to the larger one
                if arrival_time_secs > departure_time_secs:
                    arrival_time = departure_time
                else:
                    departure_time = arrival_time

            stop_point = passing_time.xpath(
                "ns:StopPointInJourneyPatternRef",
                namespaces={"ns": "http://www.netex.org.uk/netex"},
            )[0].attrib["ref"]
            stop_point = journey_patterns_dict[pattern_ref]["stop_points"][stop_point]
            stop_point = stop_name_to_id[scheduled_stop_points_dict[stop_point]["name"]]
            stop_times.append(
                StopTime(
                    trip_id=journey_id,
                    arrival_time=arrival_time,
                    departure_time=departure_time,
                    stop_id=stop_point,
                    stop_sequence=i + 1,
                )
            )
            previous_departure_hour = departure_hour

        # Generate the shape for the trip
        hash = "|".join(passed_service_links)
        hash = f"SI:SI0:Shape:{hashlib.sha1(hash.encode()).hexdigest()}:IJPP"
        if hash not in shape_map:
            shape_map[hash] = []
            for service_link in passed_service_links:
                for point in service_links_dict[service_link]["linestring"]:
                    shape_map[hash].append(
                        Shape(
                            shape_id=hash,
                            shape_pt_lat=point[1],
                            shape_pt_lon=point[0],
                            shape_pt_sequence=len(shape_map[hash]) + 1,
                        )
                    )

        trips.append(
            Trip(
                route_id=line_ref,
                service_id=day_type,
                trip_id=journey_id,
                shape_id=hash,
                trip_headsign=pattern["name"],
            )
        )

print("    DONE")
print("    Creating calendars...", end="")
# Create calendars
new_calendar = []
new_calendar_dates = []

day_type_to_calendar = {
    day_type.id: {
        'start_date': day_type.get_period_range()[0],
        'end_date': day_type.get_period_range()[1],
        'active_days': day_type.get_dates(),
    } for day_type in day_types_list
}

trip_to_day_types = {}
for trip in trips:
    if not trip.trip_id in trip_to_day_types:
        trip_to_day_types[trip.trip_id] = []
    trip_to_day_types[trip.trip_id].append(day_type_to_calendar[trip.service_id])

trip_id_to_service_hash = {}

# Dedupe & sort the dates
for trip_id, day_types in trip_to_day_types.items():
    active_days = sorted(list(set(chain(*[day_type['active_days'] for day_type in day_types]))))
    start_date = min(date_type['start_date'] for date_type in day_types)
    end_date = max(date_type['end_date'] for date_type in day_types)
    service_id = hashlib.sha1(','.join(active_day.strftime("%Y%m%d") for active_day in active_days).encode()).hexdigest()
    new_calendar.append(Calendar(service_id, start_date.strftime("%Y%m%d"), end_date.strftime("%Y%m%d"), [0] * 8))
    for day in active_days:
        new_calendar_dates.append(CalendarDate(service_id, day.strftime("%Y%m%d"), 1))
    trip_id_to_service_hash[trip_id] = service_id

# Update the trips with the new service ids
for trip in trips:
    trip.service_id = trip_id_to_service_hash[trip.trip_id]


print("    DONE")
print("    Quick stats:")
print("     - Stops:", len(stops))
print("     - Operators:", len(operators))
print("     - Routes:", len(routes))
print("     - Trips:", len(trips))
print("     - Shapes:", len(shape_map.keys()))
print("     - Stop times:", len(stop_times))
print("     - Calendars:", len(new_calendar))
print("     - Calendar dates:", len(new_calendar_dates))
print(
    "     - Journeys/Trips with more than one day type:",
    len(journeys_with_more_than_one_day_type),
)

# Loop one more time over stop times to identify stop_time_with_arrival_before_previous_departure_time
stop_times_dict = {}
for stop_time in stop_times:
    if stop_time.trip_id not in stop_times_dict:
        stop_times_dict[stop_time.trip_id] = []
    stop_times_dict[stop_time.trip_id].append(stop_time)

# Generate a txt report of stop_times_with_arrival_before_previous_departure_time
# Format:
# TRIP: trip_id
# for stop_time in stop_times_with_arrival_before_previous_departure_time:
#    print(f'{stop_time.stop_id} - {stop_time.arrival_time} - {stop_time.departure_time}')
with open("stop_times_with_arrival_before_previous_departure_time.txt", "w") as f:
    for trip_id, stop_times_l in stop_times_dict.items():
        for i in range(1, len(stop_times_l)):
            if stop_times_l[i].arrival_time < stop_times_l[i - 1].departure_time:
                f.write(f"TRIP: {trip_id}\n")
                # Iterate over all stop times in the trip and highlight the problematic ones
                for j, stop_time in enumerate(stop_times_l):
                    if j == i:
                        f.write(
                            f"    {stop_time.stop_id} - {stop_time.arrival_time} - {stop_time.departure_time} <--\n"
                        )
                    else:
                        f.write(
                            f"    {stop_time.stop_id} - {stop_time.arrival_time} - {stop_time.departure_time}\n"
                        )

# Write the GTFS into a zip file
print("Writing the GTFS into a zip file...")
with tempfile.TemporaryDirectory() as tempdir:
    stops_df = pd.DataFrame([vars(stop) for stop in stops]).drop_duplicates()
    stops_df.to_csv(Path(tempdir) / "stops.txt", index=False)
    routes_df = pd.DataFrame([vars(route) for route in routes]).drop_duplicates()
    routes_df.to_csv(Path(tempdir) / "routes.txt", index=False)
    trips_df = pd.DataFrame([vars(trip) for trip in trips]).drop_duplicates()
    trips_df.to_csv(Path(tempdir) / "trips.txt", index=False)
    stop_times_df = pd.DataFrame([vars(stop_time) for stop_time in stop_times]).drop_duplicates()
    stop_times_df.to_csv(Path(tempdir) / "stop_times.txt", index=False)
    calendars_df = pd.DataFrame([vars(calendar) for calendar in new_calendar]).drop_duplicates()
    # Drop holiday column
    calendars_df = calendars_df.drop(columns=["holiday"])
    calendars_df.to_csv(Path(tempdir) / "calendar.txt", index=False)
    calendar_dates_df = pd.DataFrame(
        [vars(calendar_date) for calendar_date in new_calendar_dates]
    ).drop_duplicates()
    calendar_dates_df.to_csv(Path(tempdir) / "calendar_dates.txt", index=False)
    agency_df = pd.DataFrame([vars(operator) for operator in operators]).drop_duplicates()
    agency_df.to_csv(Path(tempdir) / "agency.txt", index=False)
    shapes_list = []
    # Flatten the list of shapes
    for shape in shape_map.values():
        for i, point in enumerate(shape):
            point.shape_pt_sequence = i
            shapes_list.append(point)
    shapes_df = pd.DataFrame([vars(shape) for shape in shapes_list]).drop_duplicates()
    shapes_df.to_csv(Path(tempdir) / "shapes.txt", index=False)
    shutil.make_archive(OUTPUT.stem, "zip", tempdir)
print("DONE")

# Clean up the cache
if CLEANUP:
    CACHE_DIR.cleanup()

# Done
print("All done! The GTFS is in", OUTPUT)
