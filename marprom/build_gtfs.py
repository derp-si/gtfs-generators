from itertools import chain
from pathlib import Path
from datetime import date, datetime, timedelta
import time
import shutil
import tempfile
import json
import zipfile
import tqdm
import requests
import pandas as pd
import csv
import hashlib
import os


OUTPUT = Path('marprom_gtfs.zip').absolute()
CACHE_DIR = tempfile.TemporaryDirectory()
os.chdir(CACHE_DIR.name)

class Route:
    def __init__(self, route_id, route_short_name, route_long_name, route_type, route_color, route_text_color):
        self.route_id = route_id
        self.route_short_name = route_short_name
        self.route_long_name = route_long_name
        self.route_type = route_type
        self.route_color = route_color
        self.route_text_color = route_text_color

    def __repr__(self):
        return f'Route({self.route_id}, {self.route_short_name}, {self.route_long_name}, {self.route_type}, {self.route_color}, {self.route_text_color})'
    
    def __str__(self):
        return f'{self.route_id} - {self.route_short_name} - {self.route_long_name}'

class Trip:
    def __init__(self, route_id, service_id, trip_id, trip_headsign, shape_id=None):
        self.route_id = route_id
        self.shape_id = shape_id
        self.service_id = service_id
        self.trip_id = trip_id
        self.trip_headsign = trip_headsign

    def __repr__(self):
        return f'Trip({self.route_id}, {self.service_id}, {self.trip_id}, {self.trip_headsign})'
    
    def __str__(self):
        return f'{self.trip_id} - {self.trip_headsign}'
    
    def set_start_time(self, start_time):
        self.start_time = start_time
        
    def set_end_time(self, end_time):
        self.end_time = end_time
    
class StopTime:
    def __init__(self, trip_id, arrival_time, departure_time, stop_id, stop_sequence):
        self.trip_id = trip_id
        self.arrival_time = arrival_time
        self.departure_time = departure_time
        self.stop_id = stop_id
        self.stop_sequence = stop_sequence
        self.timepoint = 1

    def __repr__(self):
        return f'StopTime({self.trip_id}, {self.arrival_time}, {self.departure_time}, {self.stop_id}, {self.stop_sequence})'
    
    def __str__(self):
        return f'{self.trip_id} - {self.stop_id} - {self.arrival_time}'
    
class Shape:
    def __init__(self, shape_id, shape_pt_lat, shape_pt_lon, shape_pt_sequence):
        self.shape_id = shape_id
        self.shape_pt_lat = shape_pt_lat
        self.shape_pt_lon = shape_pt_lon
        self.shape_pt_sequence = shape_pt_sequence

    def __repr__(self):
        return f'Shape({self.shape_id}, {self.shape_pt_lat}, {self.shape_pt_lon}, {self.shape_pt_sequence})'
    
    def __str__(self):
        return f'{self.shape_id} - {self.shape_pt_lat} - {self.shape_pt_lon}'


req_count = 0
MARPROM_BASE = "https://marprom-proxy.derp.si"
REQUEST_DELAY = 1

req_count += 1
stops_data = requests.get(f'{MARPROM_BASE}/OBA/GetAllStopPoints')
stops_data.raise_for_status()

#### Mapping:
## StopPointId -> stop_id
## Name -> stop_name
## Lat -> stop_lat
## Lon -> stop_lon

stops = pd.DataFrame(stops_data.json()['StopPoints'])
stops = stops.rename(columns={
    'StopPointId': 'stop_id',
    'Name': 'stop_name',
    'Lat': 'stop_lat',
    'Lon': 'stop_lon'
})

# Drop other columns
stops = stops[['stop_id', 'stop_name', 'stop_lat', 'stop_lon']]

# Prepare calendar dates
# We will use three service_ids: D (workdays), S (saturday), N (sunday & holidays)

schedule = requests.get(f'{MARPROM_BASE}/OBA/GetServiceDates')
schedule.raise_for_status()
schedule = schedule.json()

start_date = datetime.strptime(schedule['ServiceDates']['DateFrom'], '%Y-%m-%d').date()

end_date = datetime.strptime(schedule['ServiceDates']['DateTo'], '%Y-%m-%d').date()

calendar = pd.DataFrame({
    'service_id': ['D', 'S', 'N'],
    'monday': [1, 0, 0],
    'tuesday': [1, 0, 0],
    'wednesday': [1, 0, 0],
    'thursday': [1, 0, 0],
    'friday': [1, 0, 0],
    'saturday': [0, 1, 0],
    'sunday': [0, 0, 1],
    'start_date': [start_date.strftime('%Y%m%d')] * 3,
    'end_date': [end_date.strftime('%Y%m%d')] * 3
})

# If we can be cheeky and set the start date to today while still being able to get the data for all schedules we should do it (This is in response to the new G5 line)
def has_all_schedules(start_date, end_date):
    """Check if there is at least one workday, saturday and sunday in the range"""
    days = {
        'D': 0,
        'S': 0,
        'N': 0
    }
    for i in range((end_date - start_date).days + 1):
        if (start_date + timedelta(days=i)).weekday() < 5:
            days['D'] += 1
        elif (start_date + timedelta(days=i)).weekday() == 5:
            days['S'] += 1
        else:
            days['N'] += 1

if has_all_schedules(date.today(), end_date):
    start_date = date.today()

holidays = requests.get('https://podatki.gov.si/dataset/ada88e06-14a2-49c4-8748-3311822e3585/resource/eb8b25ea-5c00-4817-a670-26e1023677c6/download/seznampraznikovindelaprostihdni20002030.csv')

holidays.raise_for_status()

rdr = csv.DictReader(holidays.text.splitlines(), delimiter=';')
holiday_cache = {date(int(r['LETO']), int(r['MESEC']), int(r['DAN'])) for r in rdr if r['DELA_PROST_DAN'] == 'da'}  # noqa

calendar_dates = []


# Check if there are any holidays in the range
for i in range((end_date - start_date).days + 1):
    if start_date + timedelta(days=i) in holiday_cache:
        # Set the default service_id to be inactive and N to be active
        if (start_date + timedelta(days=i)).weekday() < 5:
            # Set the D service_id to be inactive
            calendar_dates.append({
                'service_id': 'D',
                'date': (start_date + timedelta(days=i)).strftime('%Y%m%d'),
                'exception_type': 2
            })
        elif (start_date + timedelta(days=i)).weekday() == 5:
            # Set the S service_id to be inactive
            calendar_dates.append({
                'service_id': 'S',
                'date': (start_date + timedelta(days=i)).strftime('%Y%m%d'),
                'exception_type': 2
            })
        else:
            continue
        # Set the N service_id to be active
        calendar_dates.append({
            'service_id': 'N',
            'date': (start_date + timedelta(days=i)).strftime('%Y%m%d'),
            'exception_type': 1
        })
        
calendar_dates = pd.DataFrame({
    'service_id': [cd['service_id'] for cd in calendar_dates],
    'date': [cd['date'] for cd in calendar_dates],
    'exception_type': [cd['exception_type'] for cd in calendar_dates]
})

# Select three dates, one for each service_id which we will use to fetch the data
start_day = start_date.weekday()
workday = start_date + timedelta(days=(0 - start_day) % 7)
saturday = start_date + timedelta(days=(5 - start_day) % 7)
sunday = start_date + timedelta(days=(6 - start_day) % 7)

routes_arr = []
route_stops_dict = {}
trips_arr = []
stop_times_arr = []
shapes_arr = []

def is_bright_color(color):
    r, g, b = int(color[:2], 16), int(color[2:4], 16), int(color[4:], 16)
    return r * 0.299 + g * 0.587 + b * 0.114 > 186

for day, service_id in zip([workday, saturday, sunday], ['D', 'S', 'N']):
    print(f'Fetching routes for {day.strftime("%Y-%m-%d")} ({service_id})')
    distinct_headsigns = []
    stops_to_visit = []
    
    # Get routes for the day
    req_count += 1
    routes_data = requests.get(f'{MARPROM_BASE}/OBA/GetLines?date={day.strftime("%Y-%m-%d")}')
    routes_data.raise_for_status()
    routes_data = routes_data.json()

    if not 'Lines' in routes_data:
        continue

    lines_aggregated = [line['LineId'] for line in routes_data['Lines']]
    lines_short_name = [line['Code'] for line in routes_data['Lines']]
    lines_color = [line['Color'].strip() for line in routes_data['Lines']]  # strip() because colors contain '\r\n' for some reason

    for line_id, line_short_name, line_color in tqdm.tqdm(zip(lines_aggregated, lines_short_name, lines_color), total=len(lines_aggregated)):
        req_count += 1
        routes = requests.get(f'{MARPROM_BASE}/OBA/GetRoutes?lineId={line_id}&Date={day.strftime("%Y-%m-%d")}&IncludeShape=true')
        routes.raise_for_status()
        routes = routes.json()
        
        routes_arr.append(Route(line_id, line_short_name, '', 3, line_color.replace('#','').upper().strip(), 'FFFFFF' if not is_bright_color(line_color.replace('#','').upper().strip()) else '000000'))

        mapped_routes = []
        for route in routes['Routes']:
            distinct_headsign = f'{route["LineId"]}/{route["HeadsignName"]}'
            if distinct_headsign in distinct_headsigns:
                continue
            distinct_headsigns.append(distinct_headsign)
            mapped_routes.append(route)
            # Add route if it doesn't exist
            if distinct_headsign not in [r.route_id for r in routes_arr]:
                #routes_arr.append(Route(distinct_headsign, line_short_name, route['HeadsignName'], 3, line_color.replace('#','').upper(), 'FFFFFF' if not is_bright_color(line_color.replace('#','').upper()) else '000000'))
                route_stops_dict[distinct_headsign] = []
                for shape_node in route['ListOfShapeNodes']:
                    shapes_arr.append(Shape(distinct_headsign, shape_node['Lat'], shape_node['Lon'], shape_node['SequenceNo']))
                    if shape_node.get('StopPointId'):
                        route_stops_dict[distinct_headsign].append(shape_node['StopPointId'])
                        stops_to_visit.append(shape_node['StopPointId']) if shape_node['StopPointId'] not in stops_to_visit else None
        
        # Get stoptimes
        req_count += 1
        stop_times = requests.get(f'{MARPROM_BASE}/OBA/GetStopPointSheduleForLine?lineId={line_id}&Date={day.strftime("%Y-%m-%d")}')
        stop_times.raise_for_status()
        stop_times = stop_times.json()

        local_stop_times = []
        for stop in stop_times['Schedules']:
            stop_point = stop['StopPoint']
            for schedule in stop['ScheduleForLine']:  
                for route_and_schedule in schedule['RouteAndSchedules']:
                    for i, departure in enumerate(route_and_schedule['Departures']):
                        trip_id = f'{service_id}/{schedule["LineId"]}/{route_and_schedule["Direction"]}/{i}'
                        # Try adding the trip to the list
                        if trip_id not in [t.trip_id for t in trips_arr]:
                            trips_arr.append(Trip(schedule["LineId"], service_id, trip_id, route_and_schedule['Direction'], f'{schedule["LineId"]}/{route_and_schedule["Direction"]}'))
                        # WTF MARPROM?
                        if stop_point['StopPointId'] == 198 and stop_point['StopPointId'] not in route_stops_dict[f'{schedule["LineId"]}/{route_and_schedule["Direction"]}']:
                            # Insert the stop at the correct position (If Melje is the first word in the Direction, insert it at the beginning, otherwise at the end)
                            if route_and_schedule['Direction'].split(' ')[0] == 'Melje':
                                route_stops_dict[f'{schedule["LineId"]}/{route_and_schedule["Direction"]}'].insert(0, stop_point['StopPointId'])
                            else:
                                route_stops_dict[f'{schedule["LineId"]}/{route_and_schedule["Direction"]}'].append(stop_point['StopPointId'])
                            # Force recalculation of already inserted stop times
                            for stop_time in stop_times_arr:
                                if stop_time.trip_id == trip_id and stop_time.stop_sequence > 0:
                                    stop_time.stop_sequence = route_stops_dict[f'{schedule["LineId"]}/{route_and_schedule["Direction"]}'].index(stop_time.stop_id) + 1
                        if stop_point['StopPointId'] not in route_stops_dict[f'{schedule["LineId"]}/{route_and_schedule["Direction"]}']:
                            print(f"Stop {stop_point['StopPointId']} ({stop_point['Name']}) not found in route {schedule['LineId']} ({route_and_schedule['Direction']})")
                            continue
                        stop_sequence = route_stops_dict[f'{schedule["LineId"]}/{route_and_schedule["Direction"]}'].index(stop_point['StopPointId'])
                        local_stop_times.append(StopTime(trip_id, f'{departure}:00', f'{departure}:00', stop_point['StopPointId'], stop_sequence + 1))

        time.sleep(REQUEST_DELAY)
        
        req_count += 1
        trips = requests.get(f'{MARPROM_BASE}/OBA/GetTrips?lineId={line_id}&Date={day.strftime("%Y-%m-%d")}&IncludeShape=true')
        trips.raise_for_status()
        trips = trips.json()

        examined_departures = []
        for trip in trips['Trips']:
                # Skip if the trip is already added
                if f"{trip['ArrivalTime']},{trip['ArrivalStopId']}" in examined_departures:
                    continue
                examined_departures.append(f"{trip['ArrivalTime']},{trip['ArrivalStopId']}")
                # Find the first stoptime for the trip
                first_stop = next((st for st in local_stop_times if st.arrival_time == f'{trip["ArrivalTime"]}:00' and st.stop_id == trip['ArrivalStopId'] and st.stop_sequence == 1), None)
                if not first_stop:
                    continue
                # get the last stop sequence
                last_stop_sequence = max([st.stop_sequence for st in local_stop_times if st.trip_id == first_stop.trip_id])
                # Insert the last stop
                local_stop_times.append(StopTime(first_stop.trip_id, f'{trip["DepartureTime"]}:00', f'{trip["DepartureTime"]}:00', trip['DepartureStopId'], last_stop_sequence + 1))
                b = 0
        stop_times_arr.extend(local_stop_times)
        # Wait for 2 seconds to avoid giving the server a hug of death
        time.sleep(REQUEST_DELAY)

print("Total requests:", req_count)

# Remove unused stops
used_stops = list(set([stop_time.stop_id for stop_time in stop_times_arr]))
stops = stops[stops['stop_id'].isin(used_stops)]

stops_df = stops

def md5_hash(value):
    """Return the MD5 hash of the given value."""
    return hashlib.md5(str(value).encode()).hexdigest()

# Hash the trip_id, route_id and shape_id so we can use it as a unique identifier
trips_df = pd.DataFrame([{'trip_id': t.trip_id.replace(' ', '_'), 'route_id': t.route_id, 'service_id': t.service_id, 'trip_headsign': t.trip_headsign, 'shape_id': t.shape_id.replace(' ', '_')} for t in trips_arr]).drop_duplicates(subset=['trip_id'])
stop_times_df = pd.DataFrame([{'trip_id': t.trip_id.replace(' ', '_'), 'arrival_time': t.arrival_time, 'departure_time': t.departure_time, 'stop_id': t.stop_id, 'stop_sequence': t.stop_sequence, 'timepoint': t.timepoint} for t in stop_times_arr])
# Sort stop times by trip_id and stop_sequence
stop_times_df = stop_times_df.sort_values(by=['trip_id', 'stop_sequence'])
# Fix the stop_sequence (recalculate it for each trip)
stop_times_df['stop_sequence'] = stop_times_df.groupby('trip_id').cumcount() + 1

shapes_df = pd.DataFrame([{'shape_id': t.shape_id.replace(' ', '_'), 'shape_pt_lat': t.shape_pt_lat, 'shape_pt_lon': t.shape_pt_lon, 'shape_pt_sequence': t.shape_pt_sequence} for t in shapes_arr]).drop_duplicates(subset=['shape_id', 'shape_pt_sequence'])
routes_df = pd.DataFrame([{'route_id': r.route_id, 'route_short_name': r.route_short_name, 'route_long_name': r.route_long_name, 'route_type': r.route_type, 'route_color': r.route_color, 'route_text_color': r.route_text_color, 'agency_id': '3'} for r in routes_arr]).drop_duplicates(subset=['route_id'])
calendar_df = calendar
calendar_dates_df = calendar_dates
agency_df = pd.DataFrame([{'agency_id': '3', 'agency_name': 'Marprom', 'agency_url': 'https://marprom.si/', 'agency_timezone': 'Europe/Ljubljana'}])

trips_df['trip_id'] = trips_df['trip_id'].apply(md5_hash)
trips_df['shape_id'] = trips_df['shape_id'].apply(md5_hash)

stop_times_df['trip_id'] = stop_times_df['trip_id'].apply(md5_hash)

shapes_df['shape_id'] = shapes_df['shape_id'].apply(md5_hash)

# Save the GTFS files
trips_df.to_csv('trips.txt', index=False)
print('trips.txt saved')
stop_times_df.to_csv('stop_times.txt', index=False)
print('stop_times.txt saved')
shapes_df.to_csv('shapes.txt', index=False)
print('shapes.txt saved')
routes_df.to_csv('routes.txt', index=False)
print('routes.txt saved')
stops_df.to_csv('stops.txt', index=False)
print('stops.txt saved')
calendar_df.to_csv('calendar.txt', index=False)
print('calendar.txt saved')
calendar_dates_df.to_csv('calendar_dates.txt', index=False)
print('calendar_dates.txt saved')
agency_df.to_csv('agency.txt', index=False)
print('agency.txt saved')


# Write the GTFS into a zip file
with zipfile.ZipFile(OUTPUT, 'w') as zipf:
    zipf.write('agency.txt')
    zipf.write('stops.txt')
    zipf.write('calendar.txt')
    zipf.write('calendar_dates.txt')
    zipf.write('routes.txt')                    
    zipf.write('trips.txt')
    zipf.write('shapes.txt')
    zipf.write('stop_times.txt')

print('GTFS saved to', OUTPUT)

b = 0

CACHE_DIR.cleanup()